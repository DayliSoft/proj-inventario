/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import de.javasoft.plaf.synthetica.SyntheticaWhiteVisionLookAndFeel;
import javax.swing.UIManager;
import vista.Frm_welcome;

/**
 *
 * @author Danilo
 */
public class main {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new SyntheticaWhiteVisionLookAndFeel());
            new controlador(new Frm_welcome()).iniciar();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
