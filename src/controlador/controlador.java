package controlador;

import com.mxrck.autocompleter.AutoCompleterCallback;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.mxrck.autocompleter.TextAutoCompleter;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
//Modelo
import modelo.*;
//Interfaz de Usuario -> VISTA
import vista.*;

public class controlador implements ActionListener, MouseListener {

    //parte C.H
    private Frm_Despacho frmDespacho = new Frm_Despacho();
    private HashMap<String, String> modeloComboBx;
    private Frm_Despacho_Listar frmDespachoListar = new Frm_Despacho_Listar();
    private Frm_Compras frmCompras = new Frm_Compras();
    // fin parte C.H
    private Frm_welcome frmWelcome;
    private Frm_inicioSesion frmInicioSesion = new Frm_inicioSesion();
    private Frm_principal frmPrincipal = new Frm_principal();
    private Frm_listarPedidos frmListarPedidos = new Frm_listarPedidos();
    private Frm_crearPedidos frmCrearPedidos = new Frm_crearPedidos();
    private Frm_usuarios frmUsuarios = new Frm_usuarios();
    private Frm_productos frmProductos = new Frm_productos();
    private Frm_locales frmLocales = new Frm_locales();
    private Frm_proveedor frmDistribuidor = new Frm_proveedor();
    private model_usuario usuario;
    ArrayList<Object> productos = new ArrayList<Object>();
    TextAutoCompleter tx;

    private modelo db = new modelo();

    public controlador(JFrame padre) {
        this.frmWelcome = (Frm_welcome) padre;
        this.frmWelcome.setVisible(true);
        this.frmWelcome.setLocationRelativeTo(null);
        hilo ejecutar = new hilo(this.frmWelcome, this.frmInicioSesion);
        ejecutar.start();
    }

    public void iniciar() {
        
        //parte C.H
        
        //Ventana principal
        this.frmPrincipal.btnNuevo.setActionCommand("nuevoDespacho");
        this.frmPrincipal.btnNuevo.addActionListener(this);
        this.frmPrincipal.btnListarGuias.setActionCommand("listarGuias");
        this.frmPrincipal.btnListarGuias.addActionListener(this);
        this.frmPrincipal.btnNuevaCompra.setActionCommand("nuevaCompra");
        this.frmPrincipal.btnNuevaCompra.addActionListener(this);
        this.frmPrincipal.btnListarCompras.setActionCommand("listarCompras");
        this.frmPrincipal.btnListarCompras.addActionListener(this);
        this.frmPrincipal.btnReceptarCompra.setActionCommand("recepcionCompra");
        this.frmPrincipal.btnReceptarCompra.addActionListener(this);
        this.frmPrincipal.btnCerrarSesion.setActionCommand("cerrarSesion");
        this.frmPrincipal.btnCerrarSesion.addActionListener(this);
        
        //Ventana despacho
        this.frmDespacho.btnBuscar.setActionCommand("desBuscarPedido");
        this.frmDespacho.btnBuscar.addActionListener(this);
        this.frmDespacho.btnGenerarGuia.setActionCommand("desGenerarGuia");
        this.frmDespacho.btnGenerarGuia.addActionListener(this);
        this.frmDespacho.txtBuscar.addKeyListener(keyBuscaPedidoDes);
        this.frmDespacho.btnCrearCompra.setActionCommand("desGenerarCompra");
        this.frmDespacho.btnCrearCompra.addActionListener(this);
        this.frmDespacho.txtNumLocal.addKeyListener(keyListenerNumeros);
        this.frmDespacho.btnAgregarProveedor.setActionCommand("desAgregarProveedor");
        this.frmDespacho.btnAgregarProveedor.addActionListener(this);
        
        //Ventana listar despacho
        this.frmDespachoListar.txtBuscar.addKeyListener(keyBuscarDespacho); 
        this.frmDespachoListar.btnBuscar.setActionCommand("buscarRangoDespacho");
        this.frmDespachoListar.btnBuscar.addActionListener(this);
        this.frmDespachoListar.btnVerReporte.setActionCommand("verReporteDespacho");
        this.frmDespachoListar.btnVerReporte.addActionListener(this);
        this.frmDespachoListar.btnEliminar.setActionCommand("borrarReporteDespacho");
        this.frmDespachoListar.btnEliminar.addActionListener(this);
        
        //Ventana compras
        this.frmCompras.txtCantidad.addKeyListener(keyAgregarCantidadCompra); 
        this.frmCompras.txtIdCompra.addKeyListener(keyBuscarCompra); 
        this.frmCompras.btnBuscarCompra.setActionCommand("comBuscarCompra");
        this.frmCompras.btnBuscarCompra.addActionListener(this);
        this.frmCompras.btnAgregar.setActionCommand("comAgregarItems");
        this.frmCompras.btnAgregar.addActionListener(this);
        this.frmCompras.btnGuardarCompra.setActionCommand("comGuardarCompra");
        this.frmCompras.btnGuardarCompra.addActionListener(this);
        this.frmCompras.btnEntregado.setActionCommand("com_Entregado");
        this.frmCompras.btnEntregado.addActionListener(this);
        this.frmCompras.btnAgotado.setActionCommand("com_Agotado");
        this.frmCompras.btnAgotado.addActionListener(this);
        this.frmCompras.btnAgregarProveedor.setActionCommand("comAgregarProveedor");
        this.frmCompras.btnAgregarProveedor.addActionListener(this);
        this.frmCompras.btnAgregarProducto.setActionCommand("comAgregarProducto");
        this.frmCompras.btnAgregarProducto.addActionListener(this);
        
        // fin parte C.H
        
        //Ventana inicio de sesion
        this.frmInicioSesion.btnIngresar.setActionCommand("ingresar");
        this.frmInicioSesion.btnIngresar.addActionListener(this);
        this.frmInicioSesion.txtUsuario.addKeyListener(keyInicioSesion); 
        this.frmInicioSesion.txtContra.addKeyListener(keyInicioSesion); 

        //Ventana principal
        this.frmPrincipal.btnListar.setActionCommand("listar");
        this.frmPrincipal.btnListar.addActionListener(this);
        this.frmPrincipal.btnCrear.setActionCommand("crear");
        this.frmPrincipal.btnCrear.addActionListener(this);
        this.frmPrincipal.btnGestionUsuarios.setActionCommand("gestionUsuarios");
        this.frmPrincipal.btnGestionUsuarios.addActionListener(this);
        this.frmPrincipal.btnGestionProductos.setActionCommand("gestionProductos");
        this.frmPrincipal.btnGestionProductos.addActionListener(this);
        this.frmPrincipal.btnGestionLocales.setActionCommand("gestionLocales");
        this.frmPrincipal.btnGestionLocales.addActionListener(this);
        this.frmPrincipal.btnGestionProveedor.setActionCommand("gestionDistribuidores");
        this.frmPrincipal.btnGestionProveedor.addActionListener(this);

        //Ventana crear pedidos
        this.frmCrearPedidos.btnAgregar.setActionCommand("ingresarTablaPedido");
        this.frmCrearPedidos.btnAgregar.addActionListener(this);
        this.frmCrearPedidos.txtCantidad.addKeyListener(keyListenerNumeros);
        this.frmCrearPedidos.btnConfirmarPedido.setActionCommand("confirmarPedido");
        this.frmCrearPedidos.btnConfirmarPedido.addActionListener(this);
        this.frmCrearPedidos.btnAgregarProducto.setActionCommand("pedAgregarProducto");
        this.frmCrearPedidos.btnAgregarProducto.addActionListener(this);

        //Ventana listar pedidos
        this.frmListarPedidos.txtBuscar.addKeyListener(keyBuscarPedidos);
        this.frmListarPedidos.btnVerReporte.setActionCommand("verReporte");
        this.frmListarPedidos.btnVerReporte.addActionListener(this);
        this.frmListarPedidos.btnEliminar.setActionCommand("borrarReporte");
        this.frmListarPedidos.btnEliminar.addActionListener(this);
        this.frmListarPedidos.btnBuscar.setActionCommand("buscarRango");
        this.frmListarPedidos.btnBuscar.addActionListener(this);

        //Ventana crear usuarios
        this.frmUsuarios.btnAgregarUsuario.setActionCommand("agregarOActualizarUsuario");
        this.frmUsuarios.btnAgregarUsuario.addActionListener(this);
        this.frmUsuarios.rdbSi.setActionCommand("1");
        this.frmUsuarios.RdbNo.setActionCommand("0");
        this.frmUsuarios.txtBuscar.addKeyListener(keyBuscarUsuarios);
        this.frmUsuarios.tablaUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                frmUsuarios.txtIdUsuario.setEnabled(false);
                frmUsuarios.btnToggleId.setSelected(false);
                int filaUsuario = frmUsuarios.tablaUsuarios.rowAtPoint(evt.getPoint());
                if (filaUsuario > -1) {
                    frmUsuarios.txtIdUsuario.setText(String.valueOf(frmUsuarios.tablaUsuarios.getValueAt(filaUsuario, 0)));
                    frmUsuarios.txtNombreUsuario.setText(String.valueOf(frmUsuarios.tablaUsuarios.getValueAt(filaUsuario, 1)));
                    frmUsuarios.cbbRolUsuario.setSelectedItem(String.valueOf(frmUsuarios.tablaUsuarios.getValueAt(filaUsuario, 2)));
                    if (String.valueOf(frmUsuarios.tablaUsuarios.getValueAt(filaUsuario, 3)).equals("Si")) {
                        frmUsuarios.rdbSi.setSelected(true);
                    } else {
                        frmUsuarios.RdbNo.setSelected(true);
                    }
                    
                    DefaultComboBoxModel modelo = (DefaultComboBoxModel) frmUsuarios.cbbLocales.getModel();
                    for (int i = 0; i < modelo.getSize(); i++) { 
                        modelo_local lo = ((modelo_local)modelo.getElementAt(i));
                        if(lo.getNombre().equals(String.valueOf(frmUsuarios.tablaUsuarios.getValueAt(filaUsuario, 5)))){
                            modelo.setSelectedItem(lo);
                            break;
                        }
                    }
                }
            }
        });
        this.frmUsuarios.btnActualizarPass.setActionCommand("actualizarPass");
        this.frmUsuarios.btnActualizarPass.addActionListener(this);
        this.frmUsuarios.btnToggleId.setActionCommand("habilitarId");
        this.frmUsuarios.btnToggleId.addActionListener(this);
        
        //Ventaba gestion productos
        this.frmProductos.btnAgregarProducto.setActionCommand("guardarProducto");
        this.frmProductos.btnAgregarProducto.addActionListener(this);
        this.frmProductos.txtBuscar.addKeyListener(keyBuscarProductos);
        this.frmProductos.tablaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                frmProductos.txtIdProducto.setEnabled(false);
                frmProductos.btnToggleId.setSelected(false);
                int filaProducto = frmProductos.tablaProductos.rowAtPoint(evt.getPoint());
                if (filaProducto > -1) {
                    frmProductos.txtIdProducto.setText(String.valueOf(frmProductos.tablaProductos.getValueAt(filaProducto, 0)));
                    frmProductos.txtDescProducto.setText(String.valueOf(frmProductos.tablaProductos.getValueAt(filaProducto, 1)));
                    frmProductos.txtUnidMedida.setText(String.valueOf(frmProductos.tablaProductos.getValueAt(filaProducto, 2)));
                    frmProductos.txtStockProducto.setText(String.valueOf(frmProductos.tablaProductos.getValueAt(filaProducto, 3)));
                    frmProductos.txtPvpProducto.setText(String.valueOf(frmProductos.tablaProductos.getValueAt(filaProducto, 4)));
                    frmProductos.txtPvpDistribuidor.setText(String.valueOf(frmProductos.tablaProductos.getValueAt(filaProducto, 5)));
                }
            }
        });
        this.frmProductos.btnToggleId.setActionCommand("habilitarIdProducto");
        this.frmProductos.btnToggleId.addActionListener(this);
        
        //Ventana gestion Locales
        this.frmLocales.txtBuscar.addKeyListener(keyBuscarLocales);
        this.frmLocales.tablaLocales.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                frmLocales.txtIdLocal.setEnabled(false);
                frmLocales.btnToggleId.setSelected(false);
                int filaLocal = frmLocales.tablaLocales.rowAtPoint(evt.getPoint());
                if (filaLocal > -1) {
                    frmLocales.txtIdLocal.setText(String.valueOf(frmLocales.tablaLocales.getValueAt(filaLocal, 0)));
                    frmLocales.txtNombreLocal.setText(String.valueOf(frmLocales.tablaLocales.getValueAt(filaLocal, 1)));
                    frmLocales.txtDireccionLocal.setText(String.valueOf(frmLocales.tablaLocales.getValueAt(filaLocal, 2)));
                    frmLocales.txtTelefonoLocal.setText(String.valueOf(frmLocales.tablaLocales.getValueAt(filaLocal, 3)));
                }
            }
        });
        this.frmLocales.btnToggleId.setActionCommand("habilitarIdLocal");
        this.frmLocales.btnToggleId.addActionListener(this);
        this.frmLocales.btnAgregarLocal.setActionCommand("guardarLocal");
        this.frmLocales.btnAgregarLocal.addActionListener(this);
        this.frmLocales.txtTelefonoLocal.addKeyListener(keyListenerNumeros);
        
        //Ventana gestion Distribuidores
        this.frmDistribuidor.txtBuscar.addKeyListener(keyBuscarDist);
        this.frmDistribuidor.tablaDistribuidores.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                frmDistribuidor.txtIdDist.setEnabled(false);
                frmDistribuidor.btnToggleId.setSelected(false);
                int filaDist = frmDistribuidor.tablaDistribuidores.rowAtPoint(evt.getPoint());
                if (filaDist > -1) {
                    frmDistribuidor.txtIdDist.setText(String.valueOf(frmDistribuidor.tablaDistribuidores.getValueAt(filaDist, 0)));
                    frmDistribuidor.txtNombreDist.setText(String.valueOf(frmDistribuidor.tablaDistribuidores.getValueAt(filaDist, 1)));
                    frmDistribuidor.txtDireccionDist.setText(String.valueOf(frmDistribuidor.tablaDistribuidores.getValueAt(filaDist, 2)));
                    frmDistribuidor.txtTelefonoDist.setText(String.valueOf(frmDistribuidor.tablaDistribuidores.getValueAt(filaDist, 3)));
                }
            }
        });
        this.frmDistribuidor.btnToggleId.setActionCommand("habilitarIdDist");
        this.frmDistribuidor.btnToggleId.addActionListener(this);
        this.frmDistribuidor.btnAgregarDist.setActionCommand("guardarDistribuidor");
        this.frmDistribuidor.btnAgregarDist.addActionListener(this);
        this.frmDistribuidor.txtTelefonoDist.addKeyListener(keyListenerNumeros);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        
        //parte C.H
        
        //Ventana principal
        if (e.getActionCommand().equals("nuevoDespacho")) {
            if(!frmDespacho.isVisible()){
                frmPrincipal.panelEscritorio.add(frmDespacho);
                frmDespacho.setVisible(true);
                formatoTablasDespacho(frmDespacho.tablaDespachoDisponible, "", "guia");
                formatoTablasDespacho(frmDespacho.tablaDespachoBajoStock, "", "compra");                
                            
                frmDespacho.txtBuscar.setText("");
                frmDespacho.txtBuscar.requestFocus(true);
                frmDespacho.txtNumLocal.setText("");
                frmDespacho.txtTransportista.setText("");
                frmDespacho.txtIdPedidoHidden.setText("");
                frmDespacho.txtIdPedidoHidden.setVisible(false);
                
                //Llenar combobox
                llenarComboBox(frmDespacho.cmbProveedor, "SELECT id_prov, nombre_prov FROM proveedor");
            }            
        }
        
        if (e.getActionCommand().equals("cerrarSesion")) {
            JInternalFrame[] ventanas = frmPrincipal.panelEscritorio.getAllFrames();
            for(int i = 0; i < ventanas.length; i++){
                try {
                    ventanas[i].setVisible(false);
                } catch (Exception ex) {
                    System.out.println("Error "+ex);
                }
            } 
            frmPrincipal.dispose();
            frmPrincipal.panelEscritorio.removeAll();
            frmPrincipal.panelEscritorio.revalidate();
            frmPrincipal.panelEscritorio.repaint();                      
            frmInicioSesion.setVisible(true);        
        }
        
        //Ventana despacho
        if (e.getActionCommand().equals("desBuscarPedido")) {
            evtBuscarPedido();            
        }
        
        if(e.getActionCommand().equals("desGenerarGuia")) {
            String strNumLocal = frmDespacho.txtNumLocal.getText().trim();
            String transp = frmDespacho.txtTransportista.getText();  //4          
            DefaultTableModel modeloTablaGuia = (DefaultTableModel) frmDespacho.tablaDespachoDisponible.getModel();
            
            if(strNumLocal.equals("") || transp.equals("")){
                JOptionPane.showMessageDialog(frmDespacho, "Los campos son obligatorios");
            }else{
                try { 
                    String sql = "SELECT id_guia from guia_remision order by `id_guia` desc limit 1";
                    String idGuia = generarIdDespacho(this.usuario.getId_local(), sql, "id_guia"); //1
                    
                    int numLocal = Integer.parseInt(strNumLocal);//2
                    
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();                    
                    String fecha = dateFormat.format(date);//3
                    
                    String idUsuario = this.usuario.getId_usu();//5
                    
                    String idPedido = frmDespacho.txtIdPedidoHidden.getText();//6
                    
                    //Gurda la cabecera de la guia de remision
                    boolean guardarGuia = db.guardarGuiaRemision(idGuia, numLocal, fecha, transp, idUsuario, idPedido);                    
                    
                    if(guardarGuia){
                        //Guarda el detalle de la guia de remision
                        for (int i = 0; i < modeloTablaGuia.getRowCount(); i++) {
                            String idProducto = modeloTablaGuia.getValueAt(i, 0).toString();
                            int cantidad = Integer.parseInt(modeloTablaGuia.getValueAt(i, 2).toString());
                            
                            if(!db.guardarDetalleGuiaRemision(cantidad, idGuia, idProducto))
                                throw new Exception();                        
                        }
                    
                        //Mensaje para mostrar o no el reporte
                        frmDespacho.btnGenerarGuia.setEnabled(false);
                        int resp = JOptionPane.showConfirmDialog(frmDespacho, "Guía de remisión generada exitosamente \n ¿Desea imprimir el reporte? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                        if (resp == 0) {//0 = si
                            generarReporteGuiaRemision(idGuia);                       
                            frmDespacho.txtNumLocal.setText("");
                            frmDespacho.txtTransportista.setText("");
                            frmDespacho.txtBuscar.setText("");
                            frmDespacho.txtBuscar.requestFocus(true);
                        }
                        //borrar tabla                        
                        formatoTablasDespacho(frmDespacho.tablaDespachoDisponible, "", "guia");
                    }
                }catch(NumberFormatException  nfexc) { 
                    JOptionPane.showMessageDialog(frmDespacho, "Debe ingresar un valor numérico en el campo No. local");
                }catch(Exception exc){
                    JOptionPane.showMessageDialog(frmDespacho, "Ha ocurrido un error. COD: "+exc.getMessage());
                }
            }
        }
        
        if(e.getActionCommand().equals("desGenerarCompra")) {
            String idProveedor = getValorCombo(frmDespacho.cmbProveedor); //4
            DefaultTableModel modeloTablaCompra = (DefaultTableModel) frmDespacho.tablaDespachoBajoStock.getModel();
            
            if(idProveedor.equals("0")){
                JOptionPane.showMessageDialog(frmDespacho, "Debe crear un proveedor");
            }else{
                try { 
                    String sql = "SELECT id_com from compras order by `id_com` desc limit 1";
                    String idCompra = generarIdDespacho(this.usuario.getId_local(), sql, "id_com"); //1                    
                    
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();                    
                    String fecha = dateFormat.format(date);//2
                    
                    String idUsuario = this.usuario.getId_usu();//5
                    
                    //Gurda la cabecera de la compra
                    boolean guardarCompra = db.guardarCompra(idCompra, fecha, idProveedor, idUsuario);
                    
                    if(guardarCompra){
                        //Guarda el detalle de la compra
                        for (int i = 0; i < modeloTablaCompra.getRowCount(); i++) {
                            String idProducto = modeloTablaCompra.getValueAt(i, 0).toString();
                            int cantidad = Integer.parseInt(modeloTablaCompra.getValueAt(i, 2).toString());
                            
                            db.guardarDetalleCompra(cantidad, idCompra, idProducto);
                        }
                    
                        //Mensaje para mostrar o no el reporte
                        frmDespacho.btnCrearCompra.setEnabled(false);
                        int resp = JOptionPane.showConfirmDialog(frmDespacho, "Orden de compra generada exitosamente \n ¿Desea imprimir el reporte? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                        if (resp == 0)
                            generarReporteCompra(idCompra);           
                        
                        frmDespacho.txtBuscar.setText("");
                        frmDespacho.txtBuscar.requestFocus(true);
                        frmDespacho.cmbProveedor.setEnabled(false);
                        frmDespacho.btnAgregarProveedor.setEnabled(false);
                        
                        //borrar tabla                        
                        formatoTablasDespacho(frmDespacho.tablaDespachoBajoStock, "", "compra");
                    }
                }catch(Exception exc){
                    JOptionPane.showMessageDialog(frmDespacho, "Ha ocurrido un error. COD: "+exc.getMessage());
                }
            }
        }
        
        if (e.getActionCommand().equals("listarCompras")) {
            listarDespacho("Listado de Compras", "compra");
        }
        
        if (e.getActionCommand().equals("listarGuias")) {
            listarDespacho("Listado Guías de Remisión", "guia_remision");
        }
        
        if (e.getActionCommand().equals("buscarRangoDespacho")) {
            Date fecha1 = frmDespachoListar.fecha1.getDate();
            Date fecha2 = frmDespachoListar.fecha2.getDate();
            
            if(fecha1 == null || fecha2 == null)
                JOptionPane.showMessageDialog(frmDespachoListar, "Ingrese fechas válidas");
            else
                frmDespachoListar.tablaResultados.setModel(db.getTablaDespachoResultados(new Object[] {fecha1, fecha2}, frmDespachoListar.getTipo()));
        }
        
        if (e.getActionCommand().equals("verReporteDespacho") || e.getActionCommand().equals("borrarReporteDespacho")) {
            try {
                DefaultTableModel modeloTabla = (DefaultTableModel) frmDespachoListar.tablaResultados.getModel();
                String dato = String.valueOf(modeloTabla.getValueAt(frmDespachoListar.tablaResultados.getSelectedRow(), 0));
                switch (e.getActionCommand()) {
                    case "verReporteDespacho":
                        if(frmDespachoListar.getTipo().equals("compra"))
                            generarReporteCompra(dato);
                        else
                            generarReporteGuiaRemision(dato);
                        
                        break;
                    case "borrarReporteDespacho":
                        int resp = frmDespachoListar.mensajes.showConfirmDialog(frmDespachoListar, "<html>¿Desea eliminar el registro <b>" + dato + "</b>?", "Alerta!", JOptionPane.YES_NO_OPTION);
                        if (resp == 0) {
                            String tabla = frmDespachoListar.getTipo();
                            db.eliminarDespacho(dato, tabla);
                            frmDespachoListar.tablaResultados.setModel(db.getTablaDespachoResultados(new Object[]{""}, tabla));
                        }
                        
                        break;
                }
            } catch (Exception err) {
                frmListarPedidos.mensajes.showMessageDialog(frmListarPedidos, "<html>Seleccione un <b>registro</b> primero", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if (e.getActionCommand().equals("recepcionCompra")) {
            administrarCompras("Recepción de mercadería", "recepcion");
        }
        
        if (e.getActionCommand().equals("nuevaCompra")) {
            administrarCompras("Crear orden de compra", "nueva");
        }
        
        if (e.getActionCommand().equals("comAgregarItems")) {
            evtAgregarItemCompra();
        }
        
        if (e.getActionCommand().equals("comBuscarCompra")) {
            evtBuscarDetalleCompra();
        }
        
        if (e.getActionCommand().equals("com_Entregado") || e.getActionCommand().equals("com_Agotado")) {
            try {
                DefaultTableModel modeloTabla = (DefaultTableModel) frmCompras.tablaDetalleCompra.getModel();
                modeloTabla.setValueAt(e.getActionCommand().split("_")[1], frmCompras.tablaDetalleCompra.getSelectedRow(), 5);
            } catch (Exception err) {
                JOptionPane.showMessageDialog(frmCompras, "<html>Seleccione un <b>registro</b> primero", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(e.getActionCommand().equals("comGuardarCompra")){
            if(frmCompras.txtIdCompra.isVisible()){
                evtRecepcionCompra(frmCompras.tablaDetalleCompra, "recepcion");
            }else{
                evtRecepcionCompra(frmCompras.tablaListaProductos, "nueva");
            }
        }
        
        if(e.getActionCommand().equals("comAgregarProveedor")){
            evtNuevoProveedor(frmCompras, frmCompras.cmbProveedor);
        }
        
        if(e.getActionCommand().equals("desAgregarProveedor")){
            evtNuevoProveedor(frmDespacho, frmDespacho.cmbProveedor);
        }
        
        if(e.getActionCommand().equals("comAgregarProducto")){
            evtNuevoProducto(frmCompras, frmCompras.txtBuscar, "compras");
        }
        
        if(e.getActionCommand().equals("pedAgregarProducto")){
            evtNuevoProducto(frmCrearPedidos, frmCrearPedidos.txtBuscar, "pedidos");
        }
    
        // fin parte C.H
        

        //Ventana Inicio Sesion
        if (e.getActionCommand().equals("ingresar")) {
            evtIniciarSesion();
        }
        //Ventana principal
        if (e.getActionCommand().equals("listar")) {
            if(!frmListarPedidos.isVisible()){
                try {
                    frmPrincipal.panelEscritorio.add(frmListarPedidos);
                    frmPrincipal.panelEscritorio.moveToFront(frmListarPedidos);
                    frmListarPedidos.fecha1.setDate(null);
                    frmListarPedidos.fecha2.setDate(null);
                    frmListarPedidos.setVisible(true);
                    frmListarPedidos.tablaPedidos.setModel(db.getTablaPedidos("",this.usuario.getId_local()));  
                } catch (Exception er) {
                    System.out.println(er.getMessage());
                }
            }            
        }

        if (e.getActionCommand().equals("crear")) {
            if(!frmCrearPedidos.isVisible()){
                frmPrincipal.panelEscritorio.add(frmCrearPedidos);
                frmPrincipal.panelEscritorio.moveToFront(frmCrearPedidos);
                frmCrearPedidos.setVisible(true);
                evtAutocompletarCampoProductos(frmCrearPedidos.txtBuscar, "pedidos");
            }
        }

        //Ventana crear pedidos
        if (e.getActionCommand().equals("ingresarTablaPedido")) {
            evtIngresarTablaPedido();
        }

        if (e.getActionCommand().equals("confirmarPedido")) {
            if (frmCrearPedidos.tablaListaProductos.getRowCount() > 0) {
                DefaultTableModel modeloTabla = (DefaultTableModel) frmCrearPedidos.tablaListaProductos.getModel();
                Object[] res = isTableOK(modeloTabla);
                if ((boolean) res[0]) {
                    //guardando la cabecera del pedido (numero de pedido, fecha y obtener el id del usuario)
                    String idPedido = generarId(this.usuario.getId_local());
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    db.guardarPedido(idPedido, dateFormat.format(date), this.usuario.getId_usu());
                    //guardando el detalle del pedido (los productos)
                    for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                        String idProducto = modeloTabla.getValueAt(i, 0).toString();
                        int cantidad = Integer.parseInt(modeloTabla.getValueAt(i, 3).toString());
                        if (cantidad > 0) {
                            db.guardarDetallePedido(idPedido, idProducto, cantidad);
                        }
                    }
                    //borrar tabla
                    for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                        modeloTabla.removeRow(i);
                        i -= 1;
                    }
                    int resp = frmCrearPedidos.mensajes.showConfirmDialog(frmCrearPedidos, "Pedido generado exitosamente, \n ¿Desea imprimir el reporte? ", "Alerta!", JOptionPane.YES_NO_OPTION);
                    if (resp == 0) {//0 = si
                        generarReportePedido(idPedido);
                    }
                } else {
                    frmCrearPedidos.mensajes.showMessageDialog(frmCrearPedidos, "<html><b>" + res[1].toString() + "</b> no es un numero entero", "Error!", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        
        // Listar Pedidos
        if (e.getActionCommand().equals("verReporte") || e.getActionCommand().equals("editarReporte") || e.getActionCommand().equals("borrarReporte")) {
            try {
                DefaultTableModel modeloTabla = (DefaultTableModel) frmListarPedidos.tablaPedidos.getModel();
                String dato = String.valueOf(modeloTabla.getValueAt(frmListarPedidos.tablaPedidos.getSelectedRow(), 0));
                switch (e.getActionCommand()) {
                    case "verReporte":
                        generarReportePedido(dato);
                        break;
                    case "editarReporte":
                        break;
                    case "borrarReporte":
                        int resp = frmListarPedidos.mensajes.showConfirmDialog(frmListarPedidos, "<html>¿Desea eliminar el pedido <b>" + dato + "</b>?", "Alerta!", JOptionPane.YES_NO_OPTION);
                        if (resp == 0) {//0 = si
                            db.eliminarPedido(dato);
                            frmListarPedidos.tablaPedidos.setModel(db.getTablaPedidos("",this.usuario.getId_local()));
                        }
                        break;
                }
            } catch (Exception err) {
                frmListarPedidos.mensajes.showMessageDialog(frmListarPedidos, "<html>Seleccione un <b>registro</b> primero", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }

        if (e.getActionCommand().equals("buscarRango")) {
            if(frmListarPedidos.fecha1.getDate() != null && frmListarPedidos.fecha2.getDate() != null)
                frmListarPedidos.tablaPedidos.setModel(db.getDatosPedidoxFechas(frmListarPedidos.fecha1.getDate(), frmListarPedidos.fecha2.getDate(),usuario.getId_local()));
            else
                JOptionPane.showMessageDialog(frmListarPedidos, "Ingrese una fecha válida");
        }

        //Ventana gestionar Usuarios
        if (e.getActionCommand().equals("gestionUsuarios")) {
            if(!frmUsuarios.isVisible()){
                this.frmPrincipal.panelEscritorio.add(this.frmUsuarios);
                frmPrincipal.panelEscritorio.moveToFront(this.frmUsuarios);                 
                this.frmUsuarios.setVisible(true);
                this.frmUsuarios.tablaUsuarios.setModel(db.getTablaUsuarios(""));
                this.frmUsuarios.cbbLocales.setModel(db.getLocales(""));
            }
        }

        if (e.getActionCommand().equals("agregarOActualizarUsuario")) {
            if (!this.frmUsuarios.txtIdUsuario.getText().equals("") && !this.frmUsuarios.txtNombreUsuario.getText().equals("")) {
                model_usuario us = new model_usuario();
                us.setId_usu(this.frmUsuarios.txtIdUsuario.getText());
                us.setPass(this.frmUsuarios.txtIdUsuario.getText());
                us.setNombre(this.frmUsuarios.txtNombreUsuario.getText());
                us.setRol(this.frmUsuarios.cbbRolUsuario.getSelectedItem().toString());
                us.setEs_admin(Integer.parseInt(this.frmUsuarios.btnGrupo.getSelection().getActionCommand()));
                us.setId_local(((modelo_local) this.frmUsuarios.cbbLocales.getSelectedItem()).getId());
                db.guardar_o_actualizarUs(us);
                this.frmUsuarios.tablaUsuarios.setModel(db.getTablaUsuarios(""));
            }else{
                JOptionPane.showMessageDialog(this.frmUsuarios, "<html>No se permiten <b>campos en blanco</b>", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if (e.getActionCommand().equals("actualizarPass")) {
            try {
                String id_usuario = String.valueOf(this.frmUsuarios.tablaUsuarios.getValueAt(this.frmUsuarios.tablaUsuarios.getSelectedRow(), 0));
                String nombre_usuario = String.valueOf(this.frmUsuarios.tablaUsuarios.getValueAt(this.frmUsuarios.tablaUsuarios.getSelectedRow(), 1));
                JTextField passwordActual = new JPasswordField();
                JTextField password1 = new JPasswordField();
                JTextField password2 = new JPasswordField();
                Object[] message = {
                    "Password Actual: ", passwordActual,
                    "Password Nuevo: ", password1,
                    "Confirmar Password: ", password2
                };
                int option = JOptionPane.showConfirmDialog(this.frmUsuarios, message, "Actualizar Pass: " + nombre_usuario, JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    model_usuario us = db.inicio_sesion(id_usuario,passwordActual.getText());
                    if (us != null) {
                        if(password1.getText().equals(password2.getText())){
                            db.updateUserPass(id_usuario, password1.getText());
                            JOptionPane.showMessageDialog(this.frmUsuarios, "Password actualizado exitosamente.");
                        }else{
                            JOptionPane.showMessageDialog(this.frmUsuarios, "El nuevo password no coincide, intente nuevamente");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this.frmUsuarios, "Hola, el password actual no se encuentra registrado en el sistema, verifiquelo e intente nuevamente.");
                    }
                }
            } catch (Exception error) {
                System.out.println(error.getMessage());
                JOptionPane.showMessageDialog(this.frmUsuarios, "<html>Seleccione un <b>registro</b> primero", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(e.getActionCommand().equals("habilitarId")){
            if(frmUsuarios.btnToggleId.isSelected()){
                this.frmUsuarios.txtIdUsuario.setEnabled(true);
                this.frmUsuarios.txtIdUsuario.setText("");
                this.frmUsuarios.txtNombreUsuario.setText("");
                this.frmUsuarios.RdbNo.setSelected(true);
            }else{
                this.frmUsuarios.txtIdUsuario.setEnabled(false);
            }
            
        }
        
        //Gestion Productos
        
        if(e.getActionCommand().equals("gestionProductos")){
            if(!frmProductos.isVisible()){
                try {
                    this.frmPrincipal.panelEscritorio.add(this.frmProductos);
                    frmPrincipal.panelEscritorio.moveToFront(this.frmProductos);
                    this.frmProductos.setVisible(true);
                    this.frmProductos.tablaProductos.setModel(db.getTablaProductos(""));

                } catch (Exception ErrPro) {
                    System.out.println(ErrPro.getMessage());
                }
            }
        }
        
        if(e.getActionCommand().equals("habilitarIdProducto")){
            if(frmProductos.btnToggleId.isSelected()){
            
                this.frmProductos.txtIdProducto.setText("");
                this.frmProductos.txtDescProducto.setText("");
                this.frmProductos.txtPvpProducto.setText("");
                this.frmProductos.txtUnidMedida.setText("");
                this.frmProductos.txtStockProducto.setText("");
                this.frmProductos.txtPvpDistribuidor.setText("");                
                
                String idProducto = evtGenerarIdProducto();
                
                this.frmProductos.txtIdProducto.setText(idProducto);
                
                this.frmProductos.txtDescProducto.requestFocus();
            }else{
                this.frmProductos.txtIdProducto.setEnabled(false);
            }
        }
        
        if(e.getActionCommand().equals("guardarProducto")){
            if(this.frmProductos.txtIdProducto.getText().equals("") || 
                    this.frmProductos.txtDescProducto.getText().equals("") || 
                    this.frmProductos.txtPvpProducto.getText().equals("") || 
                    this.frmProductos.txtStockProducto.getText().equals("") || 
                    this.frmProductos.txtUnidMedida.getText().equals(""))
            {
                JOptionPane.showMessageDialog(this.frmUsuarios, "Existen campos vacios", "Error!", JOptionPane.ERROR_MESSAGE);
            }else{
                model_producto pro = new model_producto();
                pro.setId_pro(this.frmProductos.txtIdProducto.getText());
                pro.setDescripcion_pro(this.frmProductos.txtDescProducto.getText());
                pro.setUnidad_medida_pro(this.frmProductos.txtUnidMedida.getText());
                pro.setStock_pro(Integer.parseInt(this.frmProductos.txtStockProducto.getText()));
                pro.setPvp_pro(Double.parseDouble(this.frmProductos.txtPvpProducto.getText().replace(",", ".")));
                pro.setPvp_distribuidor(Double.parseDouble(this.frmProductos.txtPvpDistribuidor.getText().replace(",", ".")));
                
                if(db.guardar_o_actualizarPro(pro))
                    this.frmProductos.tablaProductos.setModel(db.getTablaProductos(""));
                else
                    JOptionPane.showMessageDialog(this.frmUsuarios, "Ha ocurrido un problema al guardar el registro", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        //Gestion Locales
        if(e.getActionCommand().equals("gestionLocales")){
            if(!frmLocales.isVisible()){
                try {
                    this.frmPrincipal.panelEscritorio.add(this.frmLocales);
                    this.frmPrincipal.panelEscritorio.moveToFront(this.frmLocales);
                    this.frmLocales.setVisible(true);
                    this.frmLocales.tablaLocales.setModel(db.getTablaLocales(""));

                } catch (Exception ErrPro) {
                    System.out.println(ErrPro.getMessage());
                }
            }
        }
        
        if(e.getActionCommand().equals("habilitarIdLocal")){
            if(frmLocales.btnToggleId.isSelected()){
                this.frmLocales.txtIdLocal.setText("");
                this.frmLocales.txtNombreLocal.setText("");
                this.frmLocales.txtDireccionLocal.setText("");
                this.frmLocales.txtTelefonoLocal.setText("");
                
                int sumLocal = db.consultarUltimoLocal() + 1;
                String idLocal = sumLocal + "";
                switch (idLocal.length()) {
                    case 1:
                        idLocal = "0" + idLocal;
                        break;
                    case 2:
                        break;
                }
                
                this.frmLocales.txtIdLocal.setText(idLocal);
            }else{
                this.frmLocales.txtIdLocal.setEnabled(false);
            }
        }
        
        if(e.getActionCommand().equals("guardarLocal")){
            if(this.frmLocales.txtIdLocal.getText().equals("") || 
                    this.frmLocales.txtNombreLocal.getText().equals("") ||
                    this.frmLocales.txtDireccionLocal.getText().equals("") ||
                    this.frmLocales.txtTelefonoLocal.getText().equals(""))
            {        
                JOptionPane.showMessageDialog(this.frmLocales, "Existen campos vacios", "Error!", JOptionPane.ERROR_MESSAGE);
            }else{
                model_local local =  new model_local();
                local.setId(this.frmLocales.txtIdLocal.getText());
                local.setNombre(this.frmLocales.txtNombreLocal.getText());
                local.setDireccion(this.frmLocales.txtDireccionLocal.getText());
                local.setTelf(this.frmLocales.txtTelefonoLocal.getText());
                db.guardar_o_actualizarLocal(local);
                this.frmLocales.tablaLocales.setModel(db.getTablaLocales(""));
            }
        }
        
        if(e.getActionCommand().equals("gestionDistribuidores")){
            if(!frmDistribuidor.isVisible()){
                try {
                    this.frmPrincipal.panelEscritorio.add(this.frmDistribuidor);
                    this.frmPrincipal.panelEscritorio.moveToFront(this.frmDistribuidor);
                    this.frmDistribuidor.setVisible(true);
                    this.frmDistribuidor.tablaDistribuidores.setModel(db.getTablaDistribuidor(""));

                } catch (Exception ErrPro) {
                    System.out.println(ErrPro.getMessage());
                }
            }
        }
        
        if(e.getActionCommand().equals("habilitarIdDist")){
            if(frmDistribuidor.btnToggleId.isSelected()){
                this.frmDistribuidor.txtIdDist.setEnabled(true);
                this.frmDistribuidor.txtIdDist.setText("");
                this.frmDistribuidor.txtNombreDist.setText("");
                this.frmDistribuidor.txtDireccionDist.setText("");
                this.frmDistribuidor.txtTelefonoDist.setText("");
                this.frmDistribuidor.txtIdDist.requestFocus();
            }else{
                this.frmDistribuidor.txtIdDist.setEnabled(false);
            }
        }
        if(e.getActionCommand().equals("guardarDistribuidor")){
            if(this.frmDistribuidor.txtIdDist.getText().equals("") || 
                    this.frmDistribuidor.txtNombreDist.getText().equals("") ||
                    this.frmDistribuidor.txtDireccionDist.getText().equals("") ||
                    this.frmDistribuidor.txtTelefonoDist.getText().equals(""))
            {        
                JOptionPane.showMessageDialog(this.frmDistribuidor, "Existen campos vacios", "Error!", JOptionPane.ERROR_MESSAGE);
            }else{
                model_distribuidor distribuidor =  new model_distribuidor();
                distribuidor.setId(this.frmDistribuidor.txtIdDist.getText());
                distribuidor.setNombre(this.frmDistribuidor.txtNombreDist.getText());
                distribuidor.setDireccion(this.frmDistribuidor.txtDireccionDist.getText());
                distribuidor.setTelf(this.frmDistribuidor.txtTelefonoDist.getText());
                if(db.guardar_o_actualizarDist(distribuidor))
                    this.frmDistribuidor.tablaDistribuidores.setModel(db.getTablaDistribuidor(""));
                else
                    JOptionPane.showMessageDialog(this.frmDistribuidor, "Error al ingresar registro", "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    //Otros metodos
    
    //parte C.H
    public void formatoTablasDespacho(JTable tabla, String cod, String tipo){
        tabla.setModel(db.getTablaDespachoDisponible(cod, tipo));
        tabla.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    }
    
    public void evtBuscarPedido(){
        String id_pedido = frmDespacho.txtBuscar.getText().trim();
        if(id_pedido.equals("")){
            JOptionPane.showMessageDialog(frmDespacho, "Debe ingresar un código de pedido");
        }else{
            frmDespacho.txtIdPedidoHidden.setText(frmDespacho.txtBuscar.getText().trim());
            
            formatoTablasDespacho(frmDespacho.tablaDespachoDisponible, id_pedido, "guia");
            formatoTablasDespacho(frmDespacho.tablaDespachoBajoStock, id_pedido, "compra");
            
            //Habilitar botones de generar guia y/o crear compra
            DefaultTableModel modeloTablaGuia = (DefaultTableModel) frmDespacho.tablaDespachoDisponible.getModel();
            DefaultTableModel modeloTablaBajo = (DefaultTableModel) frmDespacho.tablaDespachoBajoStock.getModel();
            
            if(modeloTablaGuia.getRowCount() > 0)
                frmDespacho.btnGenerarGuia.setEnabled(true);            
            else
                frmDespacho.btnGenerarGuia.setEnabled(false);
            
            if(modeloTablaBajo.getRowCount() > 0){
                frmDespacho.btnCrearCompra.setEnabled(true);            
                frmDespacho.cmbProveedor.setEnabled(true);
                frmDespacho.btnAgregarProveedor.setEnabled(true);
            }else{
                frmDespacho.btnCrearCompra.setEnabled(false);
                frmDespacho.cmbProveedor.setEnabled(false);
                frmDespacho.btnAgregarProveedor.setEnabled(false);
            }
        }
    }
    
    public String generarIdDespacho(String idLocal, String sql, String id) {
        int ultPedido = db.consultarUltimoDespacho(sql, id);
        int sumPedido = ultPedido + 1;
        String idPedido = sumPedido + "";
        switch (idPedido.length()) {
            case 1:
                idPedido = "0000" + idPedido;
                break;
            case 2:
                idPedido = "000" + idPedido;
                break;
            case 3:
                idPedido = "00" + idPedido;
                break;
            case 4:
                idPedido = "0" + idPedido;
                break;
            case 5:
                break;
        }
        return idPedido + "-" + idLocal;
    }
    
    public void generarReporteGuiaRemision(String idGuia) {
        try {
            JasperReport reporte = (JasperReport) JRLoader.loadObject("src/vista/reporteGuiaRemision.jasper");

            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();
            //parametros de entrada
            parametros.put("idGuia", idGuia);
            parametros.put("usuario", this.usuario.getNombre());
            
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, db.getConexion());
            JasperViewer.viewReport(jasperPrint, false);
        } catch (JRException E) {
            System.out.println("Error: " + E.getMessage());
        }
    }
    
    public void generarReporteCompra(String idCompra) {
        try {
            JasperReport reporte = (JasperReport) JRLoader.loadObject("src/vista/reporteCompra.jasper");

            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();
            //parametros de entrada
            parametros.put("idCompra", idCompra);
            parametros.put("usuario", this.usuario.getNombre());
            
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, db.getConexion());
            JasperViewer.viewReport(jasperPrint, false);
        } catch (JRException E) {
            System.out.println("Error: " + E.getMessage());
        }
    }
    
    public void llenarComboBox(JComboBox combo, String sql){
        modeloComboBx = db.llenarComboBox(sql);
        
        if(combo.getItemCount() > 0)
            combo.removeAllItems();
        
        modeloComboBx.keySet().forEach((s) -> {
            combo.addItem(s);
        });
    }
    
    public String getValorCombo(JComboBox combo){
        try {
            return modeloComboBx.get(combo.getSelectedItem().toString()).toString();
        } catch (Exception e) {
            return "0";
        }        
    }
    
    public void listarDespacho(String titulo, String tabla){
        frmDespachoListar.setTipo(tabla);
        frmDespachoListar.lblNombreBuscar.setText("# "+tabla.split("_")[0].toUpperCase()+": ");
        frmDespachoListar.setTitle(titulo);
        
        if(!frmDespachoListar.isVisible()){
            frmPrincipal.panelEscritorio.add(frmDespachoListar);            
            frmDespachoListar.setVisible(true);
        }
        
        frmDespachoListar.txtBuscar.setText("");
        frmDespachoListar.txtBuscar.requestFocus(true);
        frmDespachoListar.fecha1.setDate(null);
        frmDespachoListar.fecha2.setDate(null);
        frmDespachoListar.tablaResultados.setModel(db.getTablaDespachoResultados(new Object[] {""}, frmDespachoListar.getTipo()));
    }
    
    public void administrarCompras(String titulo, String tipo){        
        frmCompras.setTitle(titulo);
        frmCompras.setIdCompra("");
        
        frmCompras.txtBuscar.setText("");
        frmCompras.txtCod.setText("");
        frmCompras.txtDescripcion.setText("");
        frmCompras.txtStock.setText("");
        frmCompras.txtPrecio.setText("");
        frmCompras.txtUnidad.setText("");
        frmCompras.txtIdCompra.setText("");
        frmCompras.txtCantidad.setText(""); 
        
        if(!frmCompras.isVisible()){
            frmPrincipal.panelEscritorio.add(frmCompras);            
            frmCompras.setVisible(true);
        }
        
        if(tipo.equals("nueva")){
            frmCompras.lblHtml.setText("<html> Puede editar la <b>cantidad</b> desde la tabla");
            frmCompras.lblIdCompra.setVisible(false);
            frmCompras.txtIdCompra.setVisible(false);
            frmCompras.lblMsg.setVisible(true);
            frmCompras.btnBuscarCompra.setVisible(false);
            frmCompras.txtBuscar.setEditable(true);
            frmCompras.txtCantidad.setEditable(true);
            frmCompras.btnAgregar.setEnabled(true);
            frmCompras.cmbProveedor.setEnabled(true);
            frmCompras.btnAgregarProducto.setEnabled(true);
            frmCompras.btnAgregarProveedor.setEnabled(true);
            llenarComboBox(frmCompras.cmbProveedor, "SELECT id_prov, nombre_prov FROM proveedor");
            
            frmCompras.txtBuscar.requestFocus(true);
            
            frmCompras.panelDetalleCompra.setVisible(false);
            frmCompras.panelListaProductos.setVisible(true);            
            vaciarTabla(frmCompras.tablaListaProductos);            
            
            //Autocompletar
            evtAutocompletarCampoProductos(frmCompras.txtBuscar, "compras");            
        }else{            
            frmCompras.lblHtml.setText("<html> Puede ingresar <b>precio compra y cantidad recib.</b> desde la tabla. Para cambiar el estado de <b>clic derecho</b> en una fila.");
            frmCompras.lblIdCompra.setVisible(true);
            frmCompras.txtIdCompra.setVisible(true);
            frmCompras.btnBuscarCompra.setVisible(true);
            frmCompras.txtBuscar.setEditable(false);
            frmCompras.txtCantidad.setEditable(false);
            frmCompras.btnAgregar.setEnabled(false);
            frmCompras.cmbProveedor.setEnabled(false);
            frmCompras.btnAgregarProducto.setEnabled(false);
            frmCompras.btnAgregarProveedor.setEnabled(false);
            frmCompras.txtIdCompra.requestFocus(true);
            frmCompras.lblMsg.setVisible(false);
            
            frmCompras.panelDetalleCompra.setVisible(true);
            frmCompras.panelListaProductos.setVisible(false);
            vaciarTabla(frmCompras.tablaDetalleCompra);
        }        
    }
    
    public void evtAgregarItemCompra(){
        if (!frmCompras.txtDescripcion.getText().equals("")) {
            if (frmCompras.txtCantidad.getText().trim().equals("") || !esNumero(frmCompras.txtCantidad.getText().trim(), "int")) {
                JOptionPane.showMessageDialog(frmCompras, "Ingrese una cantidad por favor.");
                frmCompras.txtCantidad.requestFocus();
            } else {
                Object[] pro = new Object[]{frmCompras.txtCod.getText(), frmCompras.txtDescripcion.getText(), frmCompras.txtUnidad.getText(), frmCompras.txtCantidad.getText()};
                Object[] res = pasarATabla(pro);
                if ((boolean) res[0]) {
                    frmCompras.txtBuscar.setText("");
                    frmCompras.txtDescripcion.setText("");
                    frmCompras.txtCod.setText("");
                    frmCompras.txtCantidad.setText("");
                    frmCompras.txtUnidad.setText("");
                    frmCompras.txtPrecio.setText("");
                    frmCompras.txtStock.setText("");
                    
                    frmCompras.txtBuscar.requestFocus();
                } else {
                    JOptionPane.showMessageDialog(frmCrearPedidos, "<html><b>" + res[1].toString() + "</b> no es un numero entero", "Error!", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            frmCompras.txtBuscar.requestFocus();
        }
    }
    
    public Object[] pasarATabla(Object vector[]) {
        //validamos si ya existe el producto en la tabla, en caso de ser asi solo se suma las cantidades
        boolean repetido = false;
        DefaultTableModel temp = (DefaultTableModel) frmCompras.tablaListaProductos.getModel();
        try {
            for (int i = 0; i < temp.getRowCount(); i++) {
                if (temp.getValueAt(i, 0).toString().equals(vector[0].toString())) {
                    int val1 = Integer.parseInt(temp.getValueAt(i, 3).toString());
                    int val2 = Integer.parseInt(vector[3].toString());
                    temp.setValueAt((val1 + val2), i, 3);
                    repetido = true;
                }
            }
            if (repetido == false) {
                temp.addRow(vector);
            }
            return new Object[]{true};
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            String[] msg = e.getMessage().trim().split("\"");
            return new Object[]{false, msg[1]};
        }
    }
    
    public void evtBuscarDetalleCompra(){
        String idCompra = frmCompras.txtIdCompra.getText().trim();
        frmCompras.tablaDetalleCompra.setModel(db.getTablaDetalleCompra(frmCompras.tablaDetalleCompra, idCompra));
        
        DefaultTableModel modeloTabla = (DefaultTableModel) frmCompras.tablaDetalleCompra.getModel();
        if(modeloTabla.getRowCount() > 0)
            frmCompras.setIdCompra(idCompra);
    }
    
    public boolean esNumero(String n, String tipo){       
        boolean estado = true;
        try{
            switch(tipo){
                case "int":
                    int i = Integer.parseInt(n); 
                    if(i <= 0)
                        estado = false;
                    break;
                case "double":
                    double d = Double.parseDouble(n); 
                    if(d <= 0)
                        estado = false;
                    break;
                case "float":
                    float f = Float.parseFloat(n);
                    if(f <= 0)
                        estado = false;
                    break;
            }                       
        }catch (Exception e) {
            estado = false;
        }
        return estado;
    }
    
    public void vaciarTabla(JTable tabla){
        DefaultTableModel modeloTabla = (DefaultTableModel) tabla.getModel();
        if(modeloTabla.getRowCount() > 0){
            for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                modeloTabla.removeRow(i);
                i -= 1;
            }
        }        
    }
    
    public Object[] validarCabeceraCompra(String tipo){
        String idCompra;
        
        if(tipo.equals("nueva")){
            String idProveedor = getValorCombo(frmCompras.cmbProveedor);
            if(idProveedor.equals("0")){
                JOptionPane.showMessageDialog(frmCompras, "Debe crear un proveedor");
                return new Object[]{false};
            }else{
                idCompra = generarIdDespacho(this.usuario.getId_local(), "SELECT id_com from compras order by `id_com` desc limit 1", "id_com");
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                return new Object[]{db.guardarCompra(idCompra, dateFormat.format(date), idProveedor, this.usuario.getId_usu()), idCompra};
            }            
        }else{
            idCompra = frmCompras.getIdCompra();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            return new Object[]{db.actualizarCompra("UPDATE compras SET fecha_ingreso_com = ? WHERE id_com = ?", new String[]{dateFormat.format(date), idCompra}), idCompra};
        }
    }
    
    public void evtRecepcionCompra(JTable tabla, String tipo){        
        String mensaje = "Orden de Compra generada exitosamente, \n ¿Desea imprimir el reporte? ";
        if (tabla.getRowCount() > 0) {
            DefaultTableModel modeloTabla = (DefaultTableModel) tabla.getModel();
            Object[] res = esTablaValida(modeloTabla, tipo);
            if ((boolean) res[0]) {
                Object[] resultado = validarCabeceraCompra(tipo);
                
                if((boolean)resultado[0]){
                    String idCompra = (String)resultado[1];
                    
                    //Guarda/Actualiza el detalle de la compra
                    if(tipo.equals("nueva")){
                        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                            String idProducto = modeloTabla.getValueAt(i, 0).toString();
                            int cantidad = Integer.parseInt(modeloTabla.getValueAt(i, 3).toString());
                            db.guardarDetalleCompra(cantidad, idCompra, idProducto);
                        }                        
                    }else{
                        for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                            String idProducto = modeloTabla.getValueAt(i, 0).toString();
                            double precio_compra = Double.parseDouble(modeloTabla.getValueAt(i, 3).toString());
                            int cantidad = Integer.parseInt(modeloTabla.getValueAt(i, 4).toString());
                            String estado = modeloTabla.getValueAt(i, 5).toString();
                            db.actualizarDetalleCompra(precio_compra, cantidad, estado, idCompra, idProducto);
                        }
                        
                        mensaje = "Inventario actualizado exitosamente, \n ¿Desea imprimir la orden? ";
                    }
                    
                    //borrar tabla
                    for (int i = 0; i < modeloTabla.getRowCount(); i++) {
                        modeloTabla.removeRow(i);
                        i -= 1;
                    }

                    int resp = JOptionPane.showConfirmDialog(frmCompras, mensaje, "Alerta!", JOptionPane.YES_NO_OPTION);
                    if (resp == 0) {
                        generarReporteCompra(idCompra);
                    }else{
                        if(tipo.equals("nueva"))
                            frmCompras.txtBuscar.requestFocus(true);
                        else
                            frmCompras.txtIdCompra.requestFocus(true);
                    } 
                }else{
                    JOptionPane.showMessageDialog(frmCompras, "Ha ocurrido un error, inténtelo más tarde");
                }
            } else {
                JOptionPane.showMessageDialog(frmCompras, res[1].toString(), "Error!", JOptionPane.ERROR_MESSAGE);
            }
        }else{
            if(tipo.equals("nueva"))
                frmCompras.txtBuscar.requestFocus(true);
            else
                frmCompras.txtIdCompra.requestFocus(true);
        }
    }
    
    public Object[] esTablaValida(DefaultTableModel modelo, String tipo) {
        try {
            int col_cantidad = 4;
            double precio;
            String mensaje = "</b> no es una valor correcto";
            
            if(tipo.equals("nueva"))
                col_cantidad = 3;
            
            for (int i = 0; i < modelo.getRowCount(); i++) {
                int a = Integer.parseInt(modelo.getValueAt(i, col_cantidad)+"");
                if(!tipo.equals("nueva")){
                    precio = Double.parseDouble(modelo.getValueAt(i, 3)+"");
                    if(precio <= 0)
                        return new Object[]{false, "<html><b>"+precio+mensaje};
                    
                    if((modelo.getValueAt(i, 5)+"").equals("null"))
                        return new Object[]{false, "Ingrese un estado, debe dar clic derecho en una fila"};
                }
                    
                if(a < 0)
                    return new Object[]{false, "<html><b>"+a+mensaje};
                
                if (a==0)
                    if (tipo.equals("nueva"))
                        return new Object[]{false, "<html><b>"+a+mensaje};
            }
            return new Object[]{true};
        } catch (NumberFormatException e) {
            return new Object[]{false, "No se permiten espacios vacíos"};
        }
    }

    public void evtNuevoProveedor(Component ventana, JComboBox combo){        
        JTextField ruc = new JTextField();
        JTextField nombre = new JTextField();
        JTextField direccion = new JTextField();
        JTextField telefono = new JTextField();
        
        Object[] message = {
            "Ruc: ", ruc,
            "Nombre: ", nombre,
            "Dirección: ", direccion,
            "Teléfono: ", telefono
        };
        int option = JOptionPane.showConfirmDialog(ventana, message, "Agregar distribuidor", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            String oRuc = ruc.getText().trim();
            String oNombre = nombre.getText().trim();
            String oDireccion = direccion.getText().trim();
            String oTelefono = telefono.getText().trim();
            if (!oRuc.equals("") && !oNombre.equals("") && !oDireccion.equals("") && !oTelefono.equals("")) {
                model_distribuidor prov = new model_distribuidor(oRuc, oNombre, oDireccion, oTelefono);
                if(db.guardar_o_actualizarDist(prov))                
                    llenarComboBox(combo, "SELECT id_prov, nombre_prov FROM proveedor");
                else
                    JOptionPane.showMessageDialog(ventana, "Ha ocurrido un error, verifique la información ingresada");
            } else {
                JOptionPane.showMessageDialog(ventana, "Existen algunos campos vacíos");
            }
        }
    }
    
    public void evtNuevoProducto(Component ventana, JTextField textAuto, String tipo){ 
        JTextField descripcion = new JTextField();
        JTextField unidad = new JTextField();
        JTextField stock = new JTextField();
        JTextField pvp = new JTextField();
        JTextField precProv = new JTextField();
        
        Object[] message = {
            "Descripción: ", descripcion,
            "Unid. de medida: ", unidad,
            "Stock: ", stock,
            "Precio venta: ", pvp,
            "Precio proveedor: ", precProv
        };
        int option = JOptionPane.showConfirmDialog(ventana, message, "Agregar producto", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            String oId = evtGenerarIdProducto();
            String oDescripcion = descripcion.getText().trim();
            String oUnidad = unidad.getText().trim();
            String oStock = stock.getText().trim();
            String oPvp= pvp.getText().trim().replace(",", ".");
            String oPrecioProv = precProv.getText().trim().replace(",", ".");
            
            if (!oDescripcion.equals("") && !oUnidad.equals("") && !oStock.equals("") && !oPvp.equals("") && !oPrecioProv.equals("")) {
                if(esNumero(oStock, "int") && esNumero(oPvp, "double") && esNumero(oPrecioProv, "double")){
                    model_producto pro = new model_producto();
                    pro.setId_pro(oId);
                    pro.setDescripcion_pro(oDescripcion);
                    pro.setUnidad_medida_pro(oUnidad);
                    pro.setStock_pro(Integer.parseInt(oStock));
                    pro.setPvp_pro(Double.parseDouble(oPvp));
                    pro.setPvp_distribuidor(Double.parseDouble(oPrecioProv));
                    
                    if(db.guardar_o_actualizarPro(pro))                
                        evtAutocompletarCampoProductos(textAuto, tipo);
                    else
                        JOptionPane.showMessageDialog(ventana, "Ha ocurrido un error, verifique la información ingresada");
                }else{
                    JOptionPane.showMessageDialog(ventana, "Ingrese sólo números en campos cantidad, precio venta y precio proveedor");
                }
            } else {
                JOptionPane.showMessageDialog(ventana, "Existen algunos campos vacíos");
            }
        }
    }
    
    private void evtAutocompletarCampoProductos(JTextField campo, String ventana) {
        try {
            if(tx.getItems().length > 0)
                tx.removeAllItems();
        } catch (Exception e) {
            System.out.println("Autocompletar esta vacio");
        }
                
        tx = new TextAutoCompleter(campo, new AutoCompleterCallback() {
            @Override
            public void callback(Object obj) {
                model_producto prod = (model_producto) obj;

                if(ventana.equals("pedidos")){                    
                    frmCrearPedidos.txtDescripcion.setText(prod.getDescripcion_pro());
                    frmCrearPedidos.txtCod.setText(prod.getId_pro());
                    frmCrearPedidos.txtUnidad.setText(prod.getUnidad_medida_pro());
                    frmCrearPedidos.txtCantidad.requestFocus();
                }else{
                    frmCompras.txtDescripcion.setText(prod.getDescripcion_pro());
                    frmCompras.txtCod.setText(prod.getId_pro());
                    frmCompras.txtUnidad.setText(prod.getUnidad_medida_pro());
                    frmCompras.txtStock.setText(""+prod.getStock_pro());
                    frmCompras.txtPrecio.setText(""+prod.getPvp_pro());

                    frmCompras.txtCantidad.requestFocus(true);
                }    
            }
        });

        if(ventana.equals("pedidos"))
            this.productos = db.getAllProductos();
        else
            this.productos = db.getInfoProductos();

        tx.addItems(this.productos);
        tx.setMode(0);
    }
    
    // fin parte C.H
    
    public void evtIniciarSesion(){
        model_usuario us = db.inicio_sesion(this.frmInicioSesion.txtUsuario.getText(), this.frmInicioSesion.txtContra.getText());
        if (us != null) {
            frmInicioSesion.dispose();
            JOptionPane.showMessageDialog(null, "Hola bienvenido " + us.getNombre());
            this.usuario = us;
            if(this.usuario.getEs_admin()==1){ //el usuario es admin
                this.frmPrincipal.menuPedido.setVisible(false);
                this.frmPrincipal.btnGestionAlmacen.setVisible(true);
                this.frmPrincipal.menuDespacho.setVisible(true);
                this.frmPrincipal.menuCompras.setVisible(true);
            }else{
                this.frmPrincipal.menuPedido.setVisible(true);
                this.frmPrincipal.btnGestionAlmacen.setVisible(false);
                this.frmPrincipal.menuDespacho.setVisible(false);
                this.frmPrincipal.menuCompras.setVisible(false);
            }
            frmPrincipal.setLocationRelativeTo(null);
            frmPrincipal.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(frmInicioSesion, "Hola, no existe el usuario registrado en el sistema, contacte al administrador");
        }
    }
    
    public void evtIngresarTablaPedido(){
        if (!frmCrearPedidos.txtDescripcion.getText().equals("")) {
            if (frmCrearPedidos.txtCantidad.getText().equals("")) {
                frmCrearPedidos.mensajes.showMessageDialog(frmCrearPedidos, "Ingrese una cantidad por favor.");
                frmCrearPedidos.txtCantidad.requestFocus();
            } else {
                Object[] pro = new Object[]{frmCrearPedidos.txtCod.getText(), frmCrearPedidos.txtDescripcion.getText(), frmCrearPedidos.txtUnidad.getText(), frmCrearPedidos.txtCantidad.getText()};
                Object[] res = ingresarProducto(pro);
                if ((boolean) res[0]) {
                    frmCrearPedidos.txtBuscar.setText("");
                    frmCrearPedidos.txtDescripcion.setText("");
                    frmCrearPedidos.txtCod.setText("");
                    frmCrearPedidos.txtCantidad.setText("");
                    frmCrearPedidos.txtUnidad.setText("");
                } else {
                    frmCrearPedidos.mensajes.showMessageDialog(frmCrearPedidos, "<html><b>" + res[1].toString() + "</b> no es un numero entero", "Error!", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            frmCrearPedidos.txtBuscar.requestFocus();
        }
    }
    
    public Object[] ingresarProducto(Object vector[]) {
        //validamos si ya existe el producto en la tabla, en caso de ser asi solo se suma las cantidades
        boolean repetido = false;
        DefaultTableModel temp = (DefaultTableModel) frmCrearPedidos.tablaListaProductos.getModel();
        try {
            for (int i = 0; i < temp.getRowCount(); i++) {
                if (temp.getValueAt(i, 0).toString().equals(vector[0].toString())) {
                    int val1 = Integer.parseInt(temp.getValueAt(i, 3).toString());
                    int val2 = Integer.parseInt(vector[3].toString());
                    temp.setValueAt((val1 + val2), i, 3);
                    repetido = true;
                }
            }
            if (repetido == false) {
                temp.addRow(vector);
            }
            return new Object[]{true};
        } catch (Exception e) {
           return new Object[]{false, "El caracter ingresado "}; 
        }
    }

    public Object[] isTableOK(DefaultTableModel modelo) {//valida si las cantidades de las tablas son numeros
        try {
            for (int i = 0; i < modelo.getRowCount(); i++) {
                if((""+modelo.getValueAt(i, 3)).equals(""))
                    return new Object[]{false, "0"};
                
                if((modelo.getValueAt(i, 3)+"").equals("null"))                   
                    return new Object[]{false, "0"};
                
                int a = Integer.parseInt(modelo.getValueAt(i, 3).toString());
                if(a <= 0)
                    return new Object[]{false, ""+a};
            }
            return new Object[]{true};
        } catch (NumberFormatException e) {
            System.out.println("Error: " + e.getMessage());//Error: For input string: "4t" -> muestra q hay un error en un numero ingresado
            String[] msg = e.getMessage().trim().split("\"");
            return new Object[]{false, msg[1]};//obtengo solo el "4g" para mostrar el error al usuario
        }
    }

    public String generarId(String idLocal) {
        int ultPedido = db.consultarUltimoPedido(idLocal);
        int sumPedido = ultPedido + 1;
        String idPedido = sumPedido + "";
        switch (idPedido.length()) {
            case 1:
                idPedido = "0000" + idPedido;
                break;
            case 2:
                idPedido = "000" + idPedido;
                break;
            case 3:
                idPedido = "00" + idPedido;
                break;
            case 4:
                idPedido = "0" + idPedido;
                break;
            case 5:
                break;
        }
        return idPedido + "-" + idLocal;
    }

    public void generarReportePedido(String idPedido) {
        try {
            JasperReport reporte = (JasperReport) JRLoader.loadObject("src/vista/reportePedidos.jasper");

            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.clear();
            //parametros de entrada
            parametros.put("idPedido", idPedido);
            parametros.put("usuario", this.usuario.getNombre());
            //-----------------------------------
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, db.getConexion());
            JasperViewer.viewReport(jasperPrint, false);
        } catch (JRException E) {
            System.out.println("Error: " + E.getMessage());
        }
    }
    
    public String evtGenerarIdProducto() {
        int sumProducto = db.consultarUltimoProducto() + 1;
        String idProducto = sumProducto + "";
        switch (idProducto.length()) {
            case 1:
                idProducto = "0000" + idProducto;
                break;
            case 2:
                idProducto = "000" + idProducto;
                break;
            case 3:
                idProducto = "00" + idProducto;
                break;
            case 4:
                idProducto = "0" + idProducto;
                break;
            case 5:
                break;
        }
        return "P"+idProducto;
    }
    
    //Eventos teclado y mouse de objetos
    
    //parte C.H
    KeyListener keyBuscaPedidoDes = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent evt) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                evtBuscarPedido();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    };
    
    KeyAdapter keyBuscarDespacho = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            frmDespachoListar.tablaResultados.setModel(db.getTablaDespachoResultados(new String[] {frmDespachoListar.txtBuscar.getText().trim()}, frmDespachoListar.getTipo()));
        }
    };
    
    KeyListener keyAgregarCantidadCompra = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent evt) {
            int k = (int) evt.getKeyChar();
            if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
                evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            }
            if (k == 241 || k == 209 || k == 45) {
                evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                evtAgregarItemCompra();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    };
    
    KeyListener keyBuscarCompra = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent evt) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                evtBuscarDetalleCompra();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    };
    
    
    // fin parte C.H
    
    KeyListener keyInicioSesion = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent evt) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                evtIniciarSesion();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    };
    
    KeyListener keyListenerNumeros = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent evt) {
            int k = (int) evt.getKeyChar();
            if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
                evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            }
            if (k == 241 || k == 209 || k == 45) {
                evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                evtIngresarTablaPedido();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    };

    KeyAdapter keyBuscarPedidos = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            frmListarPedidos.tablaPedidos.setModel(db.getTablaPedidos(frmListarPedidos.txtBuscar.getText(),usuario.getId_local()));
        }
    };

    KeyAdapter keyBuscarUsuarios = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            frmUsuarios.tablaUsuarios.setModel(db.getTablaUsuarios(frmUsuarios.txtBuscar.getText()));
        }
    };
    
    KeyAdapter keyBuscarProductos = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            frmProductos.tablaProductos.setModel(db.getTablaProductos(frmProductos.txtBuscar.getText()));
        }
    };
    
    KeyAdapter keyBuscarLocales = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            frmLocales.tablaLocales.setModel(db.getTablaLocales(frmLocales.txtBuscar.getText()));
        }
    };
    
    KeyAdapter keyBuscarDist = new KeyAdapter() {
        @Override
        public void keyReleased(KeyEvent e) {
            frmDistribuidor.tablaDistribuidores.setModel(db.getTablaDistribuidor(frmDistribuidor.txtBuscar.getText()));
        }
    };

    //Clases modidificadas
    
    //parte C.H
    
    // fin parte C.H   
    
    
    class hilo extends Thread {

        Frm_welcome frm_welcome;
        Frm_inicioSesion frm_inicioSesion;
        public int auxiliar = 0;
        public boolean realizado = false;

        hilo(JFrame ventana, JFrame frm_inicioSesion) {
            this.frm_welcome = (Frm_welcome) ventana;
            this.frm_inicioSesion = (Frm_inicioSesion) frm_inicioSesion;
            if (realizado == false) {
                realizado = true;
                this.frm_welcome.barra.setMaximum(49);
                this.frm_welcome.barra.setMinimum(0);
                this.frm_welcome.barra.setStringPainted(true);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    auxiliar++;
                    this.frm_welcome.barra.setValue(auxiliar);
                    this.frm_welcome.repaint();
                    switch (auxiliar) {
                        case 3:
                            this.frm_welcome.text.setText("Cargando programa...");
                            break;
                        case 20:
                            this.frm_welcome.text.setText("Leyendo preferencias");
                            break;
                        case 50:
                            this.frm_welcome.text.setText("Carga finalizada");
                            break;
                        case 60:
                            try {
                                frm_inicioSesion.setLocationRelativeTo(null);
                                this.frm_welcome.dispose();
                                frm_inicioSesion.setVisible(true);
                            } catch (Exception e) {
                                System.out.println(e);
                            }
                            break;
                    }
                    Thread.sleep(40);
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Frm_welcome.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
