/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

public class model_producto {
    private String id_pro;
    private String descripcion_pro;
    private String unidad_medida_pro;
    private int stock_pro;
    private double pvp_pro;
    private int cantidad;
    private double pvp_distribuidor;

    public model_producto(String id_pro, String descripcion_pro, String unidad_medida_pro) {
        this.id_pro = id_pro;
        this.descripcion_pro = descripcion_pro;
        this.unidad_medida_pro = unidad_medida_pro;
        this.stock_pro = 0;
        this.pvp_pro = 0.0;
    }

    public model_producto(String id_pro, String unidad_medida_pro, int stock_pro, double pvp_pro) {
        this.id_pro = id_pro;
        this.unidad_medida_pro = unidad_medida_pro;
        this.stock_pro = stock_pro;
        this.pvp_pro = pvp_pro;
    }

    public model_producto() {
        this.id_pro = "";
        this.descripcion_pro = "";
        this.unidad_medida_pro = "";
        this.stock_pro = 0;
        this.pvp_pro = 0.0;
        this.cantidad = 0;
    }

    public double getPvp_distribuidor() {
        return pvp_distribuidor;
    }

    public void setPvp_distribuidor(double pvp_distribuidor) {
        this.pvp_distribuidor = pvp_distribuidor;
    }

    public String getId_pro() {
        return id_pro;
    }

    public void setId_pro(String id_pro) {
        this.id_pro = id_pro;
    }

    public String getDescripcion_pro() {
        return descripcion_pro;
    }

    public void setDescripcion_pro(String descripcion_pro) {
        this.descripcion_pro = descripcion_pro;
    }

    public String getUnidad_medida_pro() {
        return unidad_medida_pro;
    }

    public void setUnidad_medida_pro(String unidad_medida_pro) {
        this.unidad_medida_pro = unidad_medida_pro;
    }

    public int getStock_pro() {
        return stock_pro;
    }
    
    public int getCantidad(){
        return cantidad;
    }
    
    public void setCantidad(int cantidad){
        this.cantidad = cantidad;
    }

    public void setStock_pro(int stock_pro) {
        this.stock_pro = stock_pro;
    }

    public double getPvp_pro() {
        return pvp_pro;
    }

    public void setPvp_pro(double pvp_pro) {
        this.pvp_pro = pvp_pro;
    }
    
    @Override
    public String toString(){
        return this.descripcion_pro;
    }
}
