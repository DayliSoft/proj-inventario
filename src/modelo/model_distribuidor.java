/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

public class model_distribuidor {
    
    private String id;
    private String nombre;
    private String direccion;
    private String telf;

    public model_distribuidor(String id, String nombre, String direccion, String telf) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telf = telf;
    }

    public model_distribuidor() {
        this.id = "";
        this.nombre = "";
        this.direccion = "";
        this.telf = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelf() {
        return telf;
    }

    public void setTelf(String telf) {
        this.telf = telf;
    }
    
}
