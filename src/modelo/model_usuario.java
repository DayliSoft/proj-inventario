package modelo;

public class model_usuario {

    private String id_usu;
    private String nombre;
    private String rol;
    private int es_admin;
    private String id_local;
    private String pass;

    public model_usuario() {
        this.id_usu ="";
        this.nombre = "";
        this.rol = "";
        this.es_admin = 0;
        this.id_local = "";
        this.pass = "";
    }

    public model_usuario(String id_usu, String nombre, String rol, int es_admin, String id_local) {
        this.id_usu = id_usu;
        this.nombre = nombre;
        this.rol = rol;
        this.es_admin = es_admin;
        this.id_local = id_local;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    public String getId_usu() {
        return id_usu;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRol() {
        return rol;
    }

    public String getId_local() {
        return id_local;
    }

    public void setId_local(String id_local) {
        this.id_local = id_local;
    }
    
    public void setRol(String rol) {
        this.rol = rol;
    }

    public int getEs_admin() {
        return es_admin;
    }

    public void setEs_admin(int es_admin) {
        this.es_admin = es_admin;
    }
    
    public void setId_usu(String id_usu) {
        this.id_usu = id_usu;
    }
}
