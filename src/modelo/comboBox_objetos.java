/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Carlos
 */
public class comboBox_objetos {

    private String id;
    private String valor;
    
    public comboBox_objetos(String id, String valor){
        this.id = id;
        this.valor = valor;
    }
    
    public String getId(){
        return this.id;
    }
    
    public void setId(String id){
        this.id = id;
    }
    
    public String getValor(){
        return this.valor;
    }
    
    public void setValor(String valor){
        this.valor = valor;
    }
}
