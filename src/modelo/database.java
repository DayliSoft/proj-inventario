/*
 * Clase encargada solo de la conexion a la bd
 */
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class database {

    private String bd= "bd_inventario";
    private String user = "root";
    private String password = "admin";
    String url= "jdbc:mysql://localhost/"+bd;
    
    private Connection connection = null;
    
    public database()
        {
            try { 
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(url,user,password);             
                if (connection != null) { 
                    System.out.println("Conectado a la base de datos ["+ this.bd +"]"); 
                } else { 
                    System.out.println("Conexion fallida!"); 
                } 
            } catch (Exception e) { 
                e.printStackTrace(); 
            }
        }

    public Connection getConexion() {
        return this.connection;
    }

    public void terminarConexion(){
        try {
            this.connection.close();
            System.out.println("Conexion terminada");
        } catch (SQLException ex) {
            Logger.getLogger(database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

