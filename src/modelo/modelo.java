package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class modelo extends database {

    //parte C.H
    
    public DefaultTableModel getTablaDespachoDisponible(String id_ped, String tipo) {
        int registros;
        int i = 0;
        int j = 0;
        String[] columNames = {"Código", "Descripción", "Cant.", "Stock actual"};
        DefaultTableModel tablemodelGuia = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
            
        DefaultTableModel tablemodelCompra = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };        
                
        if(!id_ped.equals("")){
            try {
                //Selecciona unicamente el pedido que no se encuentra en una guia de remision
                String sql = "SELECT detalle.*, producto.* FROM bd_inventario.detalle_pedido AS detalle, bd_inventario.productos AS producto "
                           + "WHERE id_ped = ? AND producto.id_pro = detalle.id_pro AND detalle.id_ped NOT IN "
                           + "(SELECT id_ped FROM bd_inventario.guia_remision WHERE id_ped = ?)";
                
                PreparedStatement pstm = this.getConexion().prepareStatement(sql);
                pstm.setString(1, id_ped);
                pstm.setString(2, id_ped);
                ResultSet res = pstm.executeQuery();
                
                //tamanio resulset
                res.last();
                registros = res.getRow();
                res.beforeFirst();
                
                int contDisponibles = 0;
                int contBajoStock = 0;
                
                while (res.next()) {
                    int stock = res.getInt("stock_pro");
                    int cantidad = res.getInt("cantidad");
                    
                    if(cantidad > stock)
                        contBajoStock++;
                    else
                        contDisponibles++;                                
                }
                
                res.beforeFirst();
                
                Object[][] dataGuia = new String[contDisponibles][4];
                Object[][] dataCompra = new String[contBajoStock][4];
                
                while (res.next()) {
                    int stock = res.getInt("stock_pro");
                    int cantidad =  res.getInt("cantidad");
                    
                    if(cantidad > stock){
                        dataCompra[i][0] = res.getString("id_pro");
                        dataCompra[i][1] = res.getString("descripcion_pro");
                        dataCompra[i][2] = res.getString("cantidad");
                        dataCompra[i][3] = res.getString("stock_pro");
                        
                        i++;
                    }else{
                        dataGuia[j][0] = res.getString("id_pro");
                        dataGuia[j][1] = res.getString("descripcion_pro");
                        dataGuia[j][2] = res.getString("cantidad");
                        dataGuia[j][3] = res.getString("stock_pro");
                        
                        j++;
                    }
                }
                res.close();
                tablemodelGuia.setDataVector(dataGuia, columNames);
                tablemodelCompra.setDataVector(dataCompra, columNames);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }       
        
        if(tipo.equals("guia"))
            return tablemodelGuia;
        else
            return tablemodelCompra;
    }
    
    public int consultarUltimoDespacho(String sql, String id) {
        try {            
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet readline = pstm.executeQuery();
            readline.next();
            String[] temp = readline.getString(id).split("-");
            return Integer.parseInt(temp[0]);
        } catch (NumberFormatException | SQLException e) {
            return 0;
        }
    }
    
    public boolean guardarGuiaRemision(String idGuia, int numLocal, String fecha, String transportista, String idUsuario, String idPedido){
        boolean estado = true;
        try {
            String sql = "INSERT INTO guia_remision VALUES ('"+idGuia+"', "+numLocal+", '"+fecha+"', '"+transportista+"', '"+idUsuario+"', '"+idPedido+"')";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            estado = false;
        }
        return estado;
    }
    
    public boolean guardarDetalleGuiaRemision(int cantidad, String idGuia, String idProducto) {
        boolean estado = true;
        try {
            String sql = "INSERT INTO detalle_guia VALUES ("+cantidad+", '"+idGuia+"', '"+idProducto+"')";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            estado = false;
        }
        //Restar la cantidad a stock de productos
        if(estado){
            actualizarStockProductos("-", cantidad, idProducto);
        }
        
        return estado;
    }
    
    public boolean guardarCompra(String idCompra, String fecha, String idProveedor, String idUsuario){
        boolean estado = true;
        try {
            String sql = "INSERT INTO compras (id_com, fecha_emision_com, id_prov, id_usu) VALUES ('"+idCompra+"', '"+fecha+"', '"+idProveedor+"', '"+idUsuario+"')";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            estado = false;
        }
        return estado;
    }
    
    public void guardarDetalleCompra(int cantidad, String idCompra, String idProducto) {
        try {
            String sql = "INSERT INTO detalle_compra (cantidad_solic, id_com, id_pro) VALUES ("+cantidad+", '"+idCompra+"', '"+idProducto+"')";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public boolean actualizarCompra(String sql, String[]parametros) {
        boolean estado = true;
        try {            
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.setString(1, parametros[0]);
            pstm.setString(2, parametros[1]);
            
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            estado = false;
        }
        return estado;
    }
    
    public void actualizarDetalleCompra(double precio_compra, int cantidad_recibida, String estado, String idCompra, String idProducto) {        
        boolean resultado = true;
        try {
            String sql = "UPDATE detalle_compra SET precio_com = "+precio_compra+", cantidad_recib = "+cantidad_recibida+", estado = '"+estado+"' "
                       + "WHERE id_com = '"+idCompra+"' AND id_pro = '"+idProducto+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            resultado = false;
        }
        //Sumar la cantidad a stock de productos
        if(resultado){
            actualizarStockProductos("+", cantidad_recibida, idProducto);
        }
    }
    
    public void actualizarStockProductos(String operacion, int cantidad, String id_pro){
        try {
            String sql = "UPDATE productos SET stock_pro = stock_pro "+operacion+" "+cantidad+" WHERE id_pro = '"+id_pro+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public HashMap<String, String> llenarComboBox(String sql){
        HashMap<String, String> modelo = new HashMap<String, String>();
        try {            
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            comboBox_objetos combo;
            
            while(rs.next()){
                combo = new comboBox_objetos(rs.getString(1), rs.getString(2));
                modelo.put(combo.getValor(), combo.getId());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        return modelo;
    }
    
    public DefaultTableModel getTablaDespachoResultados(Object []param, String tabla) {
        int registros;
        int i = 0;
        String lblId;
        String lblFecha;
        String sql;
        SimpleDateFormat _sdf= new SimpleDateFormat("yyyy/MM/dd");
        
        if(tabla.equals("compra")){
            lblId = "Código de compra";
            lblFecha = "Fecha de emisión";
            
            if(param.length == 1)
                sql = "SELECT id_com, fecha_emision_com FROM compras WHERE id_com LIKE \"%"+param[0]+"%\"";
            else
                sql = "SELECT id_com, fecha_emision_com FROM compras WHERE date(fecha_emision_com) BETWEEN '"+_sdf.format(param[0])+"' AND '"+_sdf.format(param[1])+"'";
            
        }else{
            lblId = "Código de guía";
            lblFecha = "Fecha de salida";
            
            if(param.length == 1)
                sql = "SELECT id_guia, fecha_salida_guia FROM guia_remision WHERE id_guia LIKE \"%"+param[0]+"%\"";        
            else
                sql = "SELECT id_guia, fecha_salida_guia FROM guia_remision WHERE date(fecha_salida_guia) BETWEEN '"+_sdf.format(param[0])+"' AND '"+_sdf.format(param[1])+"'";
        }        
                
        String[] columNames = {lblId, lblFecha};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {            
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet res = pstm.executeQuery();
            
            //tamanio resulset
            res.last();
            registros = res.getRow();
            res.beforeFirst();
            
            Object[][] data = new String[registros][2];
            
            while(res.next()){
                String fecha = res.getString(2);
                data[i][0] = res.getString(1);
                data[i][1] = fecha;
                
                i++;
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return tablemodel;
    }
    
    public void eliminarDespacho(String id, String tabla){
        try {
            String sql;
            
            if(tabla.equals("compra"))
                sql = "DELETE FROM compras WHERE id_com='"+id+"'";
            else
                sql = "DELETE FROM guia_remision WHERE id_guia='"+id+"'";
            
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error al borrar: "+e.getMessage());
        }
    }
    
    public ArrayList getInfoProductos() {
        ArrayList<model_producto> productos = new ArrayList<model_producto>();
        try {
            String sql = "SELECT id_pro, descripcion_pro, unidad_medida_pro, stock_pro, pvp_pro FROM productos";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                model_producto prod_model = new model_producto();
                prod_model.setId_pro(rs.getString("id_pro"));
                prod_model.setDescripcion_pro(rs.getString("descripcion_pro"));
                prod_model.setUnidad_medida_pro(rs.getString("unidad_medida_pro"));
                prod_model.setStock_pro(rs.getInt("stock_pro"));
                prod_model.setPvp_pro(rs.getDouble("pvp_pro"));
                
                productos.add(prod_model);
            }
            return productos;
        } catch (Exception e) {
            System.out.println("Hubo en error en la obrencion de los datos: " + e.getMessage());
            return productos;
        }
    }
    
    public DefaultTableModel getTablaDetalleCompra(JTable tabla, String idCompra) {
        int registros;
        int i = 0;
        DefaultTableModel tablemodel = (DefaultTableModel) tabla.getModel();
                
        try {
            //Solo compras que no se hayan receptado (fecha fecha_ingreso_com = NULL)
            String sql = "SELECT detalle.*, producto.*, compra.* FROM detalle_compra AS detalle, productos AS producto, compras AS compra "
                       + "WHERE detalle.id_com = ? AND detalle.id_pro=producto.id_pro AND compra.fecha_ingreso_com IS NULL AND compra.id_com = ?";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.setString(1, idCompra);
            pstm.setString(2, idCompra);
            ResultSet res = pstm.executeQuery();

            //tamanio resulset
            res.last();
            registros = res.getRow();
            res.beforeFirst();

            Object[][] datos = new String[registros][6];
            String nombres[] = {"Cod", "Descripción", "Cantidad solic.", "Precio compra", "Cantidad recib.", "Estado"};

            while (res.next()) {
                datos[i][0] = res.getString("id_pro");
                datos[i][1] = res.getString("descripcion_pro");
                datos[i][2] = res.getString("cantidad_solic");
                datos[i][3] = res.getString("precio_com");                
                datos[i][4] = res.getString("cantidad_recib");
                datos[i][5] = res.getString("estado");

                i++;
            }
            
            tablemodel.setDataVector(datos, nombres);

            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }        
        
        return tablemodel;
    }
    
    
    //fin parte C.H
    
    
    public model_usuario inicio_sesion(String id, String pass) {
        model_usuario us = null;
        try {
            String sql = "select count(*) as total, id_usu, nombre, rol_usu, es_admin, Id as id_local from usuarios where id_usu = ? and pass = ? ";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.setString(1, id);
            pstm.setString(2, pass);
            ResultSet readline = pstm.executeQuery();
            readline.next();
            if (readline.getInt("total") > 0) {
                us = new model_usuario(readline.getString("id_usu"), readline.getString("nombre"), readline.getString("rol_usu"), readline.getInt("es_admin"), readline.getString("id_local"));
                return us;
            } else {
                return us;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            return us;
        }
    }

    public ArrayList getAllProductos() {
        ArrayList<model_producto> productos = new ArrayList<model_producto>();
        try {
            String sql = "select id_pro, descripcion_pro, unidad_medida_pro from productos";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet readline = pstm.executeQuery();
            while (readline.next()) {
                productos.add(new model_producto(readline.getString("id_pro"), readline.getString("descripcion_pro"), readline.getString("unidad_medida_pro")));
            }
            return productos;
        } catch (Exception e) {
            System.out.println("Hubo en error en la obrencion de los datos: " + e.getMessage());
            return productos;
        }
    }
    
    public ArrayList buscarProdLike(String txtBuscar){
        ArrayList<model_producto> productos = new ArrayList<model_producto>();
        try {
            String sql = "select id_pro, descripcion_pro, unidad_medida_pro from productos where descripcion_pro like \"%"+txtBuscar+"%\"";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet readline = pstm.executeQuery();
            while (readline.next()) {
                System.out.println(readline.getString("descripcion_pro"));
                productos.add(new model_producto(readline.getString("id_pro"), readline.getString("descripcion_pro"), readline.getString("unidad_medida_pro")));
            }
            return productos;
        } catch (Exception e) {
            System.out.println("Hubo en error en la obrencion de los datos: " + e.getMessage());
            return productos;
        }
    }
    
    public void guardarPedido(String idPedido, String fecha, String idUsuario){
        try {
            String sql = "insert into pedidos values ('"+idPedido+"','"+fecha+"','"+idUsuario+"') ";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void guardarDetallePedido(String idPedido, String idProducto, int cantidad) {
        try {
            String sql = "insert into detalle_pedido values ("+cantidad+",'"+idPedido+"','"+idProducto+"')";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public int consultarUltimoPedido(String id_Local) {
        try {
            String sql ="SELECT" +
            "     bd_inventario.pedidos.id_ped AS pedidos_id_ped" +
            "FROM" +
            "     bd_inventario.usuarios INNER JOIN bd_inventario.pedidos ON bd_inventario.usuarios.id_usu = bd_inventario.pedidos.id_usu" +
            "     INNER JOIN bd_inventario.locales ON bd_inventario.usuarios.Id = bd_inventario.locales.Id where bd_inventario.locales.Id = '"+id_Local+"' order by `id_ped` desc limit 1";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet readline = pstm.executeQuery();
            readline.next();
            System.out.println(readline.getString("id_ped"));
            String[] temp = readline.getString("id_ped").split("-");
            return Integer.parseInt(temp[0]);
        } catch (NumberFormatException | SQLException e) {
            return 0;
        }
    }
    
    public DefaultTableModel getTablaPedidos(String param, String idLocal) {
        int registros = 0;
        String[] columNames = {"Id Pedidos", "Fecha"};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("Select count(*) as total FROM bd_inventario.usuarios INNER JOIN bd_inventario.pedidos ON bd_inventario.usuarios.id_usu = bd_inventario.pedidos.id_usu " +
            "INNER JOIN bd_inventario.locales ON bd_inventario.usuarios.Id = bd_inventario.locales.Id where bd_inventario.pedidos.id_ped like \"%"+param+"%\" and bd_inventario.locales.Id = '"+idLocal+"'");
            ResultSet res = pstm.executeQuery();
            res.next();
            registros = res.getInt("total");
            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Object[][] data = new String[registros][2];
            PreparedStatement pstm = this.getConexion().prepareStatement("Select * FROM bd_inventario.usuarios INNER JOIN bd_inventario.pedidos ON bd_inventario.usuarios.id_usu = bd_inventario.pedidos.id_usu " +
            "INNER JOIN bd_inventario.locales ON bd_inventario.usuarios.Id = bd_inventario.locales.Id where bd_inventario.pedidos.id_ped like \"%"+param+"%\" and bd_inventario.locales.Id = '"+idLocal+"'");
            ResultSet res = pstm.executeQuery();
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date result;
            for (int i = 0; res.next(); i++) {
                String fecha = res.getString("fecha_ped");
                //result =  dateFormat.parse(fecha);
                data[i][0] = res.getString("id_ped");
                data[i][1] = fecha;
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return tablemodel;
    }
    
    public void eliminarPedido(String idPedido){
        try {
            String sql = "delete from pedidos where id_ped='"+idPedido+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error salvaje "+e.getMessage());
        }
    }
    
    public DefaultTableModel getDatosPedidoxFechas(Date fecha1, Date fecha2, String idLocal){
        SimpleDateFormat _sdf= new SimpleDateFormat("yyyy/MM/dd");
        System.out.println(_sdf.format(fecha1));
        int registros = 0;
        String[] columNames = {"Id Pedidos", "Fecha"};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("SELECT count(*) as total FROM bd_inventario.usuarios INNER JOIN bd_inventario.pedidos ON bd_inventario.usuarios.id_usu = bd_inventario.pedidos.id_usu " +
            "INNER JOIN bd_inventario.locales ON bd_inventario.usuarios.Id = bd_inventario.locales.Id WHERE date(fecha_ped) BETWEEN '"+_sdf.format(fecha1)+"' and '"+_sdf.format(fecha2)+"' and bd_inventario.locales.Id = '"+idLocal+"'");
            ResultSet res = pstm.executeQuery();
            res.next();
            registros = res.getInt("total");
            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Object[][] data = new String[registros][2];
            PreparedStatement pstm = this.getConexion().prepareStatement("Select * from bd_inventario.usuarios INNER JOIN bd_inventario.pedidos ON bd_inventario.usuarios.id_usu = bd_inventario.pedidos.id_usu " +
            "INNER JOIN bd_inventario.locales ON bd_inventario.usuarios.Id = bd_inventario.locales.Id WHERE date(fecha_ped) BETWEEN '"+_sdf.format(fecha1)+"' and '"+_sdf.format(fecha2)+"' and bd_inventario.locales.Id = '"+idLocal+"'");
            ResultSet res = pstm.executeQuery();
            for (int i = 0; res.next(); i++) {
                String fecha = res.getString("fecha_ped");
                //result =  dateFormat.parse(fecha);
                data[i][0] = res.getString("id_ped");
                data[i][1] = fecha;
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return tablemodel;
    }
    
    public DefaultTableModel getTablaUsuarios(String param) {
        int registros = 0;
        String[] columNames = {"Id Usuario", "Nombre", "Rol", "Admin Central", "Password", "Local"};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("Select count(*) as total FROM usuarios where id_usu like \"%"+param+"%\"or usuarios.nombre like \"%"+param+"%\"or usuarios.rol_usu like \"%"+param+"%\"");
            ResultSet res = pstm.executeQuery();
            res.next();
            registros = res.getInt("total");
            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Object[][] data = new String[registros][6];
            String resp = "select usuarios.id_usu AS usuarios_id_usu,"
                    + "usuarios.pass AS usuarios_pass,"
                    + "usuarios.nombre AS usuarios_nombre,"
                    + "usuarios.rol_usu AS usuarios_rol_usu,"
                    + "usuarios.es_admin AS usuarios_es_admin,"
                    + "usuarios.Id AS usuarios_Id," 
                    + "locales.Id AS locales_Id,"
                    + "locales.nombre AS locales_nombre from locales INNER JOIN usuarios ON locales.Id = usuarios.Id "
                    + "where "
                    + "id_usu like \"%"+param+"%\" or usuarios.nombre like \"%"+param+"%\" or usuarios.rol_usu like \"%"+param+"%\"";
            PreparedStatement pstm = this.getConexion().prepareStatement(resp);
            ResultSet res = pstm.executeQuery();
            for (int i = 0; res.next(); i++) {
                data[i][0] = res.getString("usuarios_id_usu");
                data[i][1] = res.getString("usuarios_nombre");
                data[i][2] = res.getString("usuarios_rol_usu");
                if(res.getString("usuarios_es_admin").equals("1")){
                    data[i][3] = "Si";
                }else{
                    data[i][3] = "No";
                }
                data[i][4] = "*********";
                data[i][5] = res.getString("locales_nombre");
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return tablemodel;
    }
    
    public DefaultComboBoxModel getLocales(String param){
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("Select * from locales where nombre like \"%"+param+"%\"");
            ResultSet res = pstm.executeQuery();
            for (int i = 0; res.next(); i++) {
                modelo.addElement(new modelo_local(res.getString("Id"), res.getString("nombre")));
            }
            res.close();
            return modelo;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return modelo;
        }
    }

    public void guardar_o_actualizarUs(model_usuario us) {
        try {
            String sql = "insert into usuarios (`id_usu`, `pass`, `nombre`, `rol_usu`, `es_admin`, `Id`) values ('"+us.getId_usu()+"','"+
                    us.getPass()+"','"+us.getNombre()+"','"+us.getRol()+"',"+us.getEs_admin()+",'"+us.getId_local()+"') ON DUPLICATE KEY UPDATE nombre='"+us.getNombre()+"',rol_usu='"+us.getRol()+"',es_admin="+us.getEs_admin()+",Id='"+us.getId_local()+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public void updateUserPass(String idUser,String newPass){
        try {
            String sql = "update usuarios set pass='"+newPass+"' where id_usu='"+idUser+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public DefaultTableModel getTablaProductos(String param) {
        int registros = 0;
        String[] columNames = {"Id Producto", "Descripcion", "Unidad medida", "Stock", "Pvp", "Precio Distribuidor"};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("Select count(*) as total FROM productos where id_pro like \"%"+param+"%\"or descripcion_pro like \"%"+param+"%\"");
            ResultSet res = pstm.executeQuery();
            res.next();
            registros = res.getInt("total");
            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Object[][] data = new String[registros][6];
            String resp = "select * from productos "
                    + "where "
                    + "id_pro like \"%"+param+"%\" or descripcion_pro like \"%"+param+"%\"";
            PreparedStatement pstm = this.getConexion().prepareStatement(resp);
            ResultSet res = pstm.executeQuery();
            for (int i = 0; res.next(); i++) {
                data[i][0] = res.getString("id_pro");
                data[i][1] = res.getString("descripcion_pro");
                data[i][2] = res.getString("unidad_medida_pro");
                data[i][3] = res.getInt("stock_pro")+"";
                data[i][4] = (res.getDouble("pvp_pro")+"").replace(".", ",");
                data[i][5] = (res.getDouble("precio_prov")+"").replace(".", ",");
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println("ErrorSQL: "+e.getMessage());
        }
        return tablemodel;
    }
    
    public int consultarUltimoProducto() {
        try {
            String sql = "SELECT id_pro from productos order by `id_pro` desc limit 1";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet readline = pstm.executeQuery();
            readline.next();
            String id = readline.getString("id_pro");
            String temp = id.substring(1,id.length());
            return Integer.parseInt(temp);
        } catch (NumberFormatException | SQLException e) {
            return 0;
        }
    }
    
    public boolean guardar_o_actualizarPro(model_producto pro) {
        boolean estado = true;
        try {
            String pvp = (pro.getPvp_pro()+"").replace(",", ".");
            String precio_prov = (pro.getPvp_distribuidor()+"").replace(",", ".");
            String sql = "insert into productos (`id_pro`, `descripcion_pro`, `unidad_medida_pro`, `stock_pro`, `pvp_pro`, `precio_prov`) values ('"+pro.getId_pro()+"','"+
                    pro.getDescripcion_pro()+"','"+pro.getUnidad_medida_pro()+"',"+pro.getStock_pro()+","+pvp+","+precio_prov+") "
                    + "ON DUPLICATE KEY UPDATE descripcion_pro='"+pro.getDescripcion_pro()+"',unidad_medida_pro='"+pro.getUnidad_medida_pro()+"',stock_pro="+pro.getStock_pro()+",pvp_pro="+pvp+",precio_prov="+precio_prov+"";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            estado = false;
        }
        return estado;
    }
    
    public DefaultTableModel getTablaLocales(String param) {
        int registros = 0;
        String[] columNames = {"Id Local", "Nombre", "Direccion", "Telefono"};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("Select count(*) as total FROM locales where Id like \"%"+param+"%\"or nombre like \"%"+param+"%\"");
            ResultSet res = pstm.executeQuery();
            res.next();
            registros = res.getInt("total");
            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Object[][] data = new String[registros][4];
            String resp = "select * from locales "
                    + "where "
                    + "Id like \"%"+param+"%\" or nombre like \"%"+param+"%\"";
            PreparedStatement pstm = this.getConexion().prepareStatement(resp);
            ResultSet res = pstm.executeQuery();
            for (int i = 0; res.next(); i++) {
                data[i][0] = res.getString("Id");
                data[i][1] = res.getString("nombre");
                data[i][2] = res.getString("direccion");
                data[i][3] = res.getString("telefono");
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println("ErrorSQL: "+e.getMessage());
        }
        return tablemodel;
    }
    
    public int consultarUltimoLocal() {
        try {
            String sql = "SELECT Id from locales order by `Id` desc limit 1";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            ResultSet readline = pstm.executeQuery();
            readline.next();
            String id = readline.getString("Id");
            return Integer.parseInt(id);
        } catch (NumberFormatException | SQLException e) {
            return 0;
        }
    }
    
    public void guardar_o_actualizarLocal(model_local local) {
        try {
            String sql = "insert into locales (`Id`, `nombre`, `direccion`, `telefono`) values ('"+local.getId()+"','"+
                    local.getNombre()+"','"+local.getDireccion()+"','"+local.getTelf()+"') "
                    + "ON DUPLICATE KEY UPDATE nombre='"+local.getNombre()+"',direccion='"+local.getDireccion()+"',telefono='"+local.getTelf()+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public DefaultTableModel getTablaDistribuidor(String param) {
        int registros = 0;
        String[] columNames = {"Id Distribuidor", "Nombre", "Direccion", "Telefono"};
        DefaultTableModel tablemodel = new DefaultTableModel() {

            @Override
            public boolean isCellEditable(int row, int column) {
                //todas las celdas no sean editables
                return false;
            }
        };
        try {
            PreparedStatement pstm = this.getConexion().prepareStatement("Select count(*) as total FROM proveedor where id_prov like \"%"+param+"%\"or nombre_prov like \"%"+param+"%\"");
            ResultSet res = pstm.executeQuery();
            res.next();
            registros = res.getInt("total");
            res.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Object[][] data = new String[registros][4];
            String resp = "select * from proveedor "
                    + "where "
                    + "id_prov like \"%"+param+"%\" or nombre_prov like \"%"+param+"%\"";
            PreparedStatement pstm = this.getConexion().prepareStatement(resp);
            ResultSet res = pstm.executeQuery();
            for (int i = 0; res.next(); i++) {
                data[i][0] = res.getString("id_prov");
                data[i][1] = res.getString("nombre_prov");
                data[i][2] = res.getString("direccion_prov");
                data[i][3] = res.getString("telefono_prov");
            }
            res.close();
            tablemodel.setDataVector(data, columNames);
        } catch (SQLException e) {
            System.out.println("ErrorSQL: "+e.getMessage());
        }
        return tablemodel;
    }

    public boolean guardar_o_actualizarDist(model_distribuidor distribuidor) {
        boolean estado = true;
        try {
            String sql = "insert into proveedor (`id_prov`, `nombre_prov`, `direccion_prov`, `telefono_prov`) values ('"+distribuidor.getId()+"','"+
                    distribuidor.getNombre()+"','"+distribuidor.getDireccion()+"','"+distribuidor.getTelf()+"') "
                    + "ON DUPLICATE KEY UPDATE nombre_prov='"+distribuidor.getNombre()+"',direccion_prov='"+distribuidor.getDireccion()+"',telefono_prov='"+distribuidor.getTelf()+"'";
            PreparedStatement pstm = this.getConexion().prepareStatement(sql);
            pstm.execute();
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            estado = false;
        }
        return estado;
    }
}
